﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ChessServer
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost host = new ServiceHost(typeof(ChessService));

            try
            {
                host.Open();
                Console.WriteLine($"Server opened at {host.Description.Endpoints[0].Address}\nPress enter to close server.\n");
                Console.ReadLine();

                host.Close();
            }
            catch(Exception e)
            {
                Console.WriteLine($"A server error has occurred: {e.Message} | Stacktrace:\n{e.StackTrace}");
                Console.ReadLine();
            }
        }
    }
}
