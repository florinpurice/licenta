﻿using ChessWcfInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ChessServer
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerSession)]
    public class ChessService : IChessService
    {
        private static readonly Dictionary<IChessClientCallback, string> RegisteredNames = new Dictionary<IChessClientCallback, string>();
        private static readonly Dictionary<int, DetailedGameInfo> AvailableGames = new Dictionary<int, DetailedGameInfo>();
        private static readonly Dictionary<int, DetailedGameInfo> ActiveGames = new Dictionary<int, DetailedGameInfo>();
        private static int NextId = 1;
        private static Random rg = new Random();

        public bool Connect(string name)
        {
            IChessClientCallback ccb = GetCallerCallback();
            if (RegisteredNames.ContainsValue(name))
                return false;
            else
            {
                RegisteredNames[ccb] = name;
                OperationContext.Current.Channel.Closed += new EventHandler((s, e) => HandleDisconnectedClient(ccb));
                OperationContext.Current.Channel.Faulted += new EventHandler((s, e) => HandleDisconnectedClient(ccb));
                return true;
            }
        }

        public void CreateGame(GameInfo gameInfo, string password = null)
        {
            IChessClientCallback ccb = GetCallerCallback();
            if (RegisteredNames.ContainsKey(ccb))
            {
                RemoveClientGames();
                gameInfo.HostName = RegisteredNames[ccb];
                gameInfo.Id = NextId++;
                AvailableGames[gameInfo.Id] = new DetailedGameInfo(gameInfo)
                {
                    HostCallback = GetCallerCallback(),
                    Password = password
                };
            }
        }

        public void RemoveGame(int gameId)
        {
            if (AvailableGames.ContainsKey(gameId))
            {
                DetailedGameInfo dgi = AvailableGames[gameId];
                if (dgi.HostCallback == GetCallerCallback())
                    AvailableGames.Remove(gameId);
            }
        }

        public List<GameInfo> GetGameList()
        {
            IChessClientCallback ccb = GetCallerCallback();
            List<GameInfo> list = AvailableGames.Values.ToList().Select(dgi =>
            {
                GameInfo gi = dgi.CloneGameInfo();
                gi.IsOwned = dgi.HostCallback == ccb ? false : true;
                return gi;
            }).ToList();
            return list;
        }

        public void JoinGame(int gameId, string password = null)
        {
            IChessClientCallback ccb = GetCallerCallback();
            if (RegisteredNames.ContainsKey(ccb) && AvailableGames.ContainsKey(gameId))
            {
                DetailedGameInfo dgi = AvailableGames[gameId];
                if(dgi.HostCallback != ccb && !dgi.GameInfo.IsPasswordProtected || dgi.Password == password)
                {
                    RemoveClientGames();
                    AvailableGames.Remove(gameId);
                    dgi.GameInfo.GuestName = RegisteredNames[ccb];
                    dgi.GuestCallback = GetCallerCallback();
                    if (dgi.GameInfo.HostColor != "white" && dgi.GameInfo.HostColor != "black")
                        dgi.GameInfo.HostColor = rg.Next() % 2 == 0 ? "white" : "black";
                    ActiveGames[gameId] = dgi;
                    //notificam jucatorii
                    Task.Run(() =>
                    {
                        GameInfo gi = dgi.CloneGameInfo();
                        gi.IsOwned = true;
                        dgi.HostCallback?.GameStarted(gi);
                        gi.IsOwned = false;
                        dgi.GuestCallback?.GameStarted(gi);
                    });
                }
            }
        }

        public void LeaveGame(int gameId)
        {
            if (ActiveGames.ContainsKey(gameId))
            {
                DetailedGameInfo dgi = ActiveGames[gameId];
                IChessClientCallback ccb = GetCallerCallback();
                Task.Run(() =>
                {
                    if (dgi.HostCallback == ccb)
                    {
                        dgi.GuestCallback?.OpponentLeft(gameId);
                        ActiveGames.Remove(gameId);
                    }
                    else if (dgi.GuestCallback == ccb)
                    {
                        dgi.HostCallback?.OpponentLeft(gameId);
                        ActiveGames.Remove(gameId);
                    }
                });
            }
        }

        public void MakeMove(int gameId, string move)
        {
            if (ActiveGames.ContainsKey(gameId))
            {
                DetailedGameInfo dgi = ActiveGames[gameId];
                IChessClientCallback ccb = GetCallerCallback();
                Task.Run(() =>
                {
                    if (dgi.HostCallback == ccb)
                        dgi.GuestCallback?.OpponentMoved(gameId, move);
                    else if (dgi.GuestCallback == ccb)
                        dgi.HostCallback?.OpponentMoved(gameId, move);
                });
            }
        }

        public void SendMessage(int gameId, string message)
        {
            if (ActiveGames.ContainsKey(gameId))
            {
                DetailedGameInfo dgi = ActiveGames[gameId];
                IChessClientCallback ccb = GetCallerCallback();
                Task.Run(() =>
                {
                    if (dgi.HostCallback == ccb)
                        dgi.GuestCallback?.SendMessage(gameId, dgi.GameInfo.HostName, message);
                    else if (dgi.GuestCallback == ccb)
                        dgi.HostCallback?.SendMessage(gameId, dgi.GameInfo.GuestName, message);
                });
            }
        }

        private IChessClientCallback GetCallerCallback()
        {
            return OperationContext.Current.GetCallbackChannel<IChessClientCallback>();
        }

        private void RemoveClientGames()
        {
            IChessClientCallback ccb = GetCallerCallback();
            foreach (DetailedGameInfo dgi in AvailableGames.Values.ToList())
                if (dgi.HostCallback == ccb)
                    AvailableGames.Remove(dgi.GameInfo.Id);
        }

        private static void HandleDisconnectedClient(IChessClientCallback ccb)
        {
            //clear client registered name
            RegisteredNames.Remove(ccb);
            //clear client's registered games
            foreach(DetailedGameInfo dgi in AvailableGames.Values.ToList())
                if(dgi.HostCallback == ccb)
                    AvailableGames.Remove(dgi.GameInfo.Id);
            //clear client's active games (and notifying the opponent)
            foreach (DetailedGameInfo dgi in ActiveGames.Values.ToList())
                if (dgi.HostCallback == ccb || dgi.GuestCallback == ccb)
                {
                    IChessClientCallback opponentCallback = dgi.HostCallback == ccb ? dgi.GuestCallback : dgi.HostCallback;
                    opponentCallback?.OpponentLeft(dgi.GameInfo.Id);
                    ActiveGames.Remove(dgi.GameInfo.Id);
                }
        }
    }
}
