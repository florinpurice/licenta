﻿using ChessWcfInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessServer
{
    public class DetailedGameInfo
    {
        public DetailedGameInfo() { }

        public DetailedGameInfo(GameInfo gi)
        {
            GameInfo = gi;
        }

        public GameInfo GameInfo { get; set; }
        public IChessClientCallback HostCallback { get; set; }
        public IChessClientCallback GuestCallback { get; set; }
        public string Password { get; set; }

        public GameInfo CloneGameInfo()
        {
            GameInfo gi = new GameInfo
            {
                GameName = GameInfo.GameName,
                GuestName = GameInfo.GuestName,
                HostColor = GameInfo.HostColor,
                HostName = GameInfo.HostName,
                Id = GameInfo.Id,
                Increment = GameInfo.Increment,
                IsPasswordProtected = GameInfo.IsPasswordProtected,
                Time = GameInfo.Time
            };
            return gi;
        }
    }
}
