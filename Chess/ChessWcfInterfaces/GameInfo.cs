﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ChessWcfInterfaces
{
    [DataContract]
    public class GameInfo
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string GameName { get; set; }

        [DataMember]
        public string HostName { get; set; }

        [DataMember]
        public string GuestName { get; set; }

        [DataMember]
        public string HostColor { get; set; }

        [DataMember]
        public bool IsPasswordProtected { get; set; }

        [DataMember]
        public uint Time { get; set; }

        [DataMember]
        public uint Increment { get; set; }

        [DataMember]
        public bool IsOwned { get; set; }
    }
}
