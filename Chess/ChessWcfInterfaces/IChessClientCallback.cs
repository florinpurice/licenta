﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ChessWcfInterfaces
{
    public interface IChessClientCallback
    {
        [OperationContract(IsOneWay = true)]
        void OpponentMoved(int gameId, string move);

        [OperationContract(IsOneWay = true)]
        void OpponentLeft(int gameId);

        [OperationContract(IsOneWay = true)]
        void GameStarted(GameInfo gameInfo);

        [OperationContract(IsOneWay = true)]
        void SendMessage(int gameId, string from, string message);
    }
}
