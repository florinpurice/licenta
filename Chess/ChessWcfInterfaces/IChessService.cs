﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ChessWcfInterfaces
{
    [ServiceContract(CallbackContract = typeof(IChessClientCallback), SessionMode = SessionMode.Required)]
    public interface IChessService
    {
        [OperationContract]
        bool Connect(string name);

        [OperationContract]
        List<GameInfo> GetGameList();

        [OperationContract]
        void CreateGame(GameInfo gameInfo, string password = null);

        [OperationContract]
        void RemoveGame(int gameId);

        [OperationContract(IsOneWay = true)]
        void JoinGame(int gameId, string password = null);

        [OperationContract(IsOneWay = true)]
        void MakeMove(int gameId, string move);

        [OperationContract(IsOneWay = true)]
        void LeaveGame(int gameId);

        [OperationContract(IsOneWay = true)]
        void SendMessage(int gameId, string message);
    }
}
