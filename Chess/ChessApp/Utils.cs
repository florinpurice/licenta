﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WPFLocalizeExtension.Engine;
using WPFLocalizeExtension.Extensions;

namespace ChessApp
{
    public static class Utils
    {
        public static readonly Random RandomGenerator = new Random();

        public static string GetLocalizedString(string key, string dictionaryName = null)
        {
            if(dictionaryName == null)
            {
                LocalizeDictionary ld = LocalizeDictionary.Instance;
                object resource = ld.GetLocalizedObject(key, null, ld.Culture);
                if (resource == null)
                    return null;
                else
                    return (string)resource;
            }
            else
                return LocExtension.GetLocalizedValue<string>(Assembly.GetCallingAssembly().GetName().Name + ":" + dictionaryName + ":" + key);
        }
    }
}
