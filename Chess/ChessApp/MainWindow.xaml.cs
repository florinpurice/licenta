﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using WPFLocalizeExtension.Engine;

namespace ChessApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Frame = mainWindowFrame;
            Pages["mainmenu"] = new Pages.MainMenu();
            Frame.Navigate(MainWindow.Pages["mainmenu"]);
        }

        public static readonly Dictionary<string, Page> Pages = new Dictionary<string, Page>();

        public static Frame Frame { get; private set; }

        /// <summary>
        /// Schimba limba in care este tradusa aplicatia.
        /// </summary>
        /// <param name="culture"></param>
        public static void ChangeCulture(string culture)
        {
            try
            {
                CultureInfo ci = new CultureInfo(culture);
                LocalizeDictionary.Instance.Culture = ci;
            }
            catch { }
        }

        public static bool IsPageCurrent(string pagekey)
        {
            if (!Pages.ContainsKey(pagekey))
                return false;
            return Pages[pagekey] == Frame.Content;
        }
    }
}
