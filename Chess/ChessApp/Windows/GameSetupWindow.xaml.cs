﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ChessApp.Windows
{
    /// <summary>
    /// Interaction logic for GameSetupWindow.xaml
    /// </summary>
    public partial class GameSetupWindow : Window
    {
        public GameSetupWindow()
        {
            InitializeComponent();
        }

        public uint WTime { get; set; }
        public uint BTime { get; set; }
        public uint WInc { get; set; }
        public uint BInc { get; set; }
        public string PlayerColor { get; set; }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                WTime = uint.Parse(whiteTime.Text);
                WTime *= 1000;
            }
            catch
            {
                whiteTime.Text = string.Empty;
                errorText.Text = Utils.GetLocalizedString("WTimeError", "GameSetup");
                return;
            }
            try
            {
                BTime = uint.Parse(blackTime.Text);
                BTime *= 1000;
            }
            catch
            {
                blackTime.Text = string.Empty;
                errorText.Text = Utils.GetLocalizedString("BTimeError", "GameSetup");
                return;
            }
            try
            {
                WInc = uint.Parse(whiteIncrement.Text);
                WInc *= 1000;
            }
            catch
            {
                whiteIncrement.Text = string.Empty;
                errorText.Text = Utils.GetLocalizedString("WIncError", "GameSetup");
                return;
            }
            try
            {
                BInc = uint.Parse(blackIncrement.Text);
                BInc *= 1000;
            }
            catch
            {
                blackIncrement.Text = string.Empty;
                errorText.Text = Utils.GetLocalizedString("BIncError", "GameSetup");
                return;
            }
            if (whiteColor.IsChecked.GetValueOrDefault())
                PlayerColor = "white";
            else if (blackColor.IsChecked.GetValueOrDefault())
                PlayerColor = "black";
            else
                PlayerColor = "random";

            try { DialogResult = true; } catch { }
            Close();
        }
    }
}
