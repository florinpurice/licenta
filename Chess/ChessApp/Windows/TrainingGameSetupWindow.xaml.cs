﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ChessApp.Windows
{
    /// <summary>
    /// Interaction logic for TrainingGameSetupWindow.xaml
    /// </summary>
    public partial class TrainingGameSetupWindow : Window
    {
        public TrainingGameSetupWindow()
        {
            InitializeComponent();
        }

        public int MoveTime { get; set; }
        public int Depth { get; set; }
        public string LimitOption { get; set; }
        public string PlayerColor { get; set; }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (whiteColor.IsChecked.GetValueOrDefault())
                PlayerColor = "white";
            else if (blackColor.IsChecked.GetValueOrDefault())
                PlayerColor = "black";
            else
                PlayerColor = "random";

            if(limitMoveTime.IsChecked.GetValueOrDefault() || limitBoth.IsChecked.GetValueOrDefault())
            {
                int mt = 0;
                if (int.TryParse(moveTime.Text, out mt) && mt > 0)
                    MoveTime = mt;
                else
                {
                    moveTime.Text = string.Empty;
                    errorText.Text = Utils.GetLocalizedString("MoveTimeError", "GameSetup");
                    return;
                }
            }
            if (limitDepth.IsChecked.GetValueOrDefault() || limitBoth.IsChecked.GetValueOrDefault())
            {
                int dp = 0;
                if (int.TryParse(depth.Text, out dp) && dp > 0)
                    Depth = dp;
                else
                {
                    depth.Text = string.Empty;
                    errorText.Text = Utils.GetLocalizedString("DepthError", "GameSetup");
                    return;
                }
            }

            if (limitMoveTime.IsChecked.GetValueOrDefault())
                LimitOption = "movetime";
            else if (limitDepth.IsChecked.GetValueOrDefault())
                LimitOption = "depth";
            else
                LimitOption = "both";

            try { DialogResult = true; } catch { }
            Close();
        }
    }
}
