﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace ChessApp.Windows
{
    /// <summary>
    /// Interaction logic for LoadEngineWindow.xaml
    /// </summary>
    public partial class LoadEngineWindow : Window
    {
        private readonly List<string> _paths = new List<string>();
        private readonly Dictionary<int, string> _dificultyLevels = new Dictionary<int, string>
        {
            { 500, "veryeasy" },
            { 1000, "easy" },
            { 1500, "medium" },
            { 2000, "hard" },
            { 2500, "veryhard" },
            { int.MaxValue, "impossible" }
        };

        public LoadEngineWindow()
        {
            InitializeComponent();
            ReadXmlFile();
        }

        public string FilePath { get; set; }

        private void ReadXmlFile()
        {
            try
            {
                _paths.Clear();
                int num = 1;
                XDocument doc = XDocument.Load("Engines/Predefined.xml");
                foreach(XElement engine in doc.Root.Elements("engine"))
                {
                    int elo = 0;
                    int.TryParse(engine.Attribute("elo").Value, out elo);
                    string name = engine.Attribute("name").Value;
                    string file = engine.Attribute("file").Value;
                    string dificulty = GetDificultyStringForElo(elo);
                    string content = num + ". " + dificulty + " (" + name + " - " + elo + " elo)";
                    Button button = new Button{ Content = content };
                    button.Click += predefinedButton_Click;
                    predefinedEnginesStackPanel.Children.Add(button);
                    _paths.Add(file);
                    ++num;
                }
            }
            catch { }
        }

        private string GetDificultyStringForElo(int elo)
        {
            List<int> limits = _dificultyLevels.Keys.ToList();
            string locKey = string.Empty;
            limits.Sort();
            for(int i = 0; i < limits.Count; ++i)
                if(elo < limits[i])
                {
                    locKey = _dificultyLevels[limits[i]];
                    break;
                }
            return Utils.GetLocalizedString(locKey, "Strings");
        }

        private void predefinedButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            string content = button.Content as string;
            int index = 0;
            int.TryParse(content.Split('.')[0], out index);
            --index;
            if(index >= 0 && index < _paths.Count)
            {
                FilePath = "Engines/" + _paths[index];
                try { DialogResult = true; } catch { }
                Close();
            }
        }

        private void loadButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog().GetValueOrDefault())
            {
                FilePath = ofd.FileName;
                try { DialogResult = true; } catch { }
                Close();
            }
        }
    }
}
