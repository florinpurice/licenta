﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ChessApp.Windows
{
    /// <summary>
    /// Interaction logic for OnlineGameSetupWindow.xaml
    /// </summary>
    public partial class OnlineGameSetupWindow : Window
    {
        public OnlineGameSetupWindow()
        {
            InitializeComponent();
        }

        public uint Time { get; set; }
        public uint Increment { get; set; }
        public string PlayerColor { get; set; }
        public string Password { get; set; }
        public bool IsPasswordRestricted { get; set; }
        public string GameName { get; set; }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            GameName = gameNameTextBox.Text;
            if (isPasswordRestrictedCb.IsChecked.GetValueOrDefault())
            {
                if (string.IsNullOrWhiteSpace(password.Password))
                {
                    errorText.Text = Utils.GetLocalizedString("PasswordError", "GameSetup");
                    return;
                }
                IsPasswordRestricted = true;
                Password = password.Password;
            }
            try
            {
                Time = uint.Parse(time.Text);
            }
            catch
            {
                time.Text = string.Empty;
                errorText.Text = Utils.GetLocalizedString("TimeError", "GameSetup");
                return;
            }
            try
            {
                Increment = uint.Parse(increment.Text);
            }
            catch
            {
                increment.Text = string.Empty;
                errorText.Text = Utils.GetLocalizedString("IncrementError", "GameSetup");
                return;
            }
            if (whiteColor.IsChecked.GetValueOrDefault())
                PlayerColor = "white";
            else if (blackColor.IsChecked.GetValueOrDefault())
                PlayerColor = "black";
            else
                PlayerColor = "random";

            try { DialogResult = true; } catch { }
            Close();
        }
    }
}
