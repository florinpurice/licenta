﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ChessApp
{
    [ValueConversion(typeof(uint), typeof(string))]
    public class UintSecondsToTimeStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            uint seconds = (uint)value;
            TimeSpan timeSpan = new TimeSpan(0, 0, (int)seconds);
            return timeSpan.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string timeString = (string)value;
            TimeSpan timeSpan = new TimeSpan();
            TimeSpan.TryParse(timeString, out timeSpan);
            return (uint)timeSpan.TotalSeconds;
        }
    }
}
