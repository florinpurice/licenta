﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ChessApp
{
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToVisibilityConverter : IValueConverter
    {
        public Visibility WhenTrue = Visibility.Visible;
        public Visibility WhenFalse = Visibility.Collapsed;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool val = (bool)value;
            bool negate = false;
            bool.TryParse(parameter as string, out negate);
            if (negate)
                val = !val;
            return val ? WhenTrue : WhenFalse;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility val = (Visibility)value;
            bool negate = false;
            bool.TryParse(parameter as string, out negate);
            if (!negate && val == Visibility.Visible || negate && val != Visibility.Visible)
                return true;
            else
                return false;
        }
    }
}
