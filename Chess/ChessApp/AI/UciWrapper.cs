﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessApp.AI
{
    public class UciWrapper : IChessEngine
    {
        private double _whiteTime;
        private double _blackTime;
        private string _ponderMove;
        private bool _isThinking = false;
        private bool _ignoreNextMove = false;

        /// <summary>
        /// Construieste un now wrapper pentru engine-ul cu binarul dat de 'enginePath'.
        /// </summary>
        /// <param name="enginePath">Locatia executabilului engine-ului.</param>
        public UciWrapper(string enginePath)
        {
            EnginePath = enginePath;
            InitEngine();
        }

        public void Dispose()
        {
            try
            {
                EngineMoved = null;
                EngineThinkingInfo = null;
                EngineProcess?.Kill();
            }
            catch { }
        }

        public event EngineEventDelegate EngineMoved;
        public event EngineEventDelegate EngineThinkingInfo;
        public event EngineEventDelegate EngineNameRecieved;

        public string EnginePath { get; }
        public Process EngineProcess { get; private set; }
        public DateTime MoveStartTime { get; private set; }
        public string EngineName { get; private set; } = "Computer";
        public int WhiteIncrement { get; private set; }
        public int BlackIncrement { get; private set; }
        public int WhiteTimeLeft
        {
            get
            {
                if (IsWhiteMove)
                    return (int)(_whiteTime - (DateTime.Now - MoveStartTime).TotalMilliseconds);
                else
                    return (int)_whiteTime;
            }
        }
        public int BlackTimeLeft
        {
            get
            {
                if (IsWhiteMove)
                    return (int)_blackTime;
                else
                    return (int)(_blackTime - (DateTime.Now - MoveStartTime).TotalMilliseconds);
            }
        }
        public readonly List<string> MoveList = new List<string>();
        public bool IsWhiteMove
        {
            get
            {
                return MoveList.Count % 2 == 0;
            }
        }
        public bool CanPonder { get; private set; } = false;
        public string MovesString
        {
            get
            {
                StringBuilder positionSb = new StringBuilder();
                foreach (string m in MoveList)
                    positionSb.Append(" " + m);
                return positionSb.ToString();
            }
        }

        public void PlayerMove(string move)
        {
            NextMoveTimer();
            MoveList.Add(move);
            if (CanPonder && _ponderMove != null)
            {
                if (_ponderMove.Equals(move))
                {
                    EngineProcess.StandardInput.WriteLine("ponderhit");
                }
                else
                {
                    _ignoreNextMove = true;
                    EngineProcess.StandardInput.WriteLine("stop");
                    string position = $"position startpos moves{MovesString}";
                    EngineProcess.StandardInput.WriteLine(position);
                    GoMove();
                    _isThinking = true;
                }
            }
            else
            {
                string position = $"position startpos moves{MovesString}";
                EngineProcess.StandardInput.WriteLine(position);
                GoMove();
                _isThinking = true;
            }
        }

        public void DoFirstMove()
        {
            if(MoveList.Count > 0)
                EngineProcess.StandardInput.WriteLine($"position startpos moves{MovesString}");
            else
                EngineProcess.StandardInput.WriteLine("position startpos");
            GoMove();
        }

        public void NewGame(int wtime, int btime, int winc, int binc)
        {
            if (_isThinking)
            {
                _ignoreNextMove = true;
                EngineProcess.StandardInput.WriteLine("stop");
            }
            EngineProcess.StandardInput.WriteLine("ucinewgame");
            MoveList.Clear();
            _ponderMove = null;
            _whiteTime = wtime;
            _blackTime = btime;
            WhiteIncrement = winc;
            BlackIncrement = binc;
            MoveStartTime = DateTime.Now;
        }

        public void SetOption(string name, string value)
        {
            EngineProcess.StandardInput.WriteLine($"setoption name {name} value {value}");
        }

        public void SendStopCommand()
        {
             _ignoreNextMove = true;
            EngineProcess.StandardInput.WriteLine("stop");
            _isThinking = false;
        }

        public void ChangeClockTimes(int wtime, int btime, int winc, int binc)
        {
            _whiteTime = wtime;
            _blackTime = btime;
            WhiteIncrement = winc;
            BlackIncrement = binc;
        }

        private void InitEngine()
        {
            EngineProcess = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    FileName = EnginePath,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true
                }
            };
            EngineProcess.Start();
            EngineProcess.OutputDataReceived += EngineOutputHandler;
            EngineProcess.BeginOutputReadLine();
            EngineProcess.StandardInput.WriteLine("uci");
            EngineProcess.StandardInput.WriteLine("isready");
        }

        private void EngineOutputHandler(object s, DataReceivedEventArgs e)
        {
            string output = e.Data;
            if (output != null)
                foreach (string line in output.Split(new[] { '\n', '\r' }))
                    if (line.StartsWith("bestmove"))
                        if (_ignoreNextMove)
                        {
                            EngineThinkingInfo?.Invoke(line + " <residual>");
                            _ignoreNextMove = false;
                        }
                        else
                        {
                            NextMoveTimer();
                            EngineThinkingInfo?.Invoke(line);

                            string[] tokens = line.Split(' ');
                            MoveList.Add(tokens[1]);
                            if (CanPonder && tokens.Length > 3)
                            {
                                _ponderMove = tokens[3];
                                string position = $"position startpos moves{MovesString} {_ponderMove}";
                                EngineProcess.StandardInput.WriteLine(position);
                                GoMove(true);
                                _isThinking = true;
                            }
                            else
                            {
                                _ponderMove = null;
                                _isThinking = false;
                            }
                            EngineMoved?.Invoke(tokens[1]);
                        }
                    else if (line.StartsWith("info"))
                    {
                        EngineThinkingInfo?.Invoke(line);
                    }
                    else if (line.StartsWith("option name Ponder"))
                    {
                        CanPonder = true;
                        SetOption("Ponder", "true");
                    }
                    else if(line.StartsWith("id name"))
                    {
                        EngineName = line.Substring(7).Trim();
                        EngineNameRecieved?.Invoke(EngineName);
                    }
        }

        private void GoMove(bool ponder = false)
        {
            string sub = ponder ? "ponder " : "";
            EngineProcess.StandardInput.WriteLine($"go {sub}time wtime {(int)_whiteTime} btime {(int)_blackTime} winc {WhiteIncrement} binc {BlackIncrement}");
        }

        private void NextMoveTimer()
        {
            if (IsWhiteMove)
            {
                _whiteTime -= (DateTime.Now - MoveStartTime).TotalMilliseconds;
                _whiteTime += WhiteIncrement;
            }
            else
            {
                _blackTime -= (DateTime.Now - MoveStartTime).TotalMilliseconds;
                _blackTime += BlackIncrement;
            }
            MoveStartTime = DateTime.Now;
        }

    }
}
