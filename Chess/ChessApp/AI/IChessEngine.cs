﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessApp.AI
{
    public delegate void EngineEventDelegate(string move);

    public interface IChessEngine : IDisposable
    {
        event EngineEventDelegate EngineMoved;

        void PlayerMove(string move);

        void NewGame(int wtime, int btime, int winc, int binc);

        void DoFirstMove();
    }
}
