﻿using ChessCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace ChessApp.AI
{
    public class SimpleEngine : IChessEngine
    {
        public int BestMoveScore;

        public bool StopSearch = false;
        public event EngineEventDelegate EngineMoved;

        public TimeSpan MoveTime { get; set; } = new TimeSpan(0, 0, 5);
        public int DepthLimit { get; set; } = -1;
        public ChessBoard Board { get; set; }
        public List<string> BestMoveChain = new List<string>();
        /// <summary>
        /// Numarul de nivele (incepand cu radacina) pentru care generarea mutarilor se face folosind valida. Un nivel asigura ca rezultatul este valid, doua nivele verifica sa nu fie mutare de pat.
        /// </summary>
        public int NumberOfValidLevels { get; set; } = 2;

        public void DoFirstMove()
        {
            if (Board != null)
                MakeMoveAsync();
        }

        public void NewGame(int wtime, int btime, int winc, int binc)
        {
            Board = new ChessBoard();
            Board.SetupStartPosition();
        }

        public void PlayerMove(string move)
        {
            if (Board != null && Board.DoMove(move))
            {

                MakeMoveAsync();
            }
        }

        public void Dispose()
        {
            StopSearch = true;
        }

        public void MakeMoveAsync()
        {
            StopSearch = false;
            Task.Run(() =>
            {
                int depth = 1;
                while(depth <= DepthLimit && !StopSearch)
                {
                    List<string> chain;
                    int score = FindBestMoveChain(Board, depth, out chain);
                    if(!StopSearch)
                    {
                        BestMoveScore = score;
                        BestMoveChain = chain;
                        ++depth;
                    }
                }
                if (BestMoveChain.Count > 0 && Board.DoMove(BestMoveChain.First()))
                    EngineMoved?.Invoke(BestMoveChain.First());
            });
            DateTime moveStartTime = DateTime.Now;
            Task.Run(() =>
            {
                while (DateTime.Now - moveStartTime < MoveTime)
                    Thread.Sleep(100);
                StopSearch = true;
            });
        }

        public int FindBestMoveChain(ChessBoard startState, int depth, out List<string> moveChain)
        {
            ChessBoard board = startState.Clone();
            if (startState.CurrentPlayerColor == PieceColor.White)
                return AlphaBetaMax(board, int.MinValue, int.MaxValue, depth, depth, out moveChain);
            else
                return AlphaBetaMin(board, int.MinValue, int.MaxValue, depth, depth, out moveChain);
        }
        
        private int AlphaBetaMax(ChessBoard boardState, int alpha, int beta, int depthLeft, int maxDepth, out List<string> bestMoveChain)
        {
            bestMoveChain = new List<string>();
            if (StopSearch)
                return 0;
            if (depthLeft == 0)
                return EvaluatePosition(boardState);
            bool forwardPlayPossible = false;
            foreach (ChessPiece piece in boardState.Pieces.Values.Where(p => p.Color == boardState.CurrentPlayerColor).ToArray())
            {
                if (StopSearch)
                    return 0;
                List<string> moves = depthLeft > maxDepth - NumberOfValidLevels
                    ?piece.PossibleMoves(boardState.Pieces)
                    :piece.PseudoPossibleMoves(boardState.Pieces);
                if(piece.GetLetterAbbreviation() == "")
                {
                    List<string> promotionMoves = new List<string>();
                    foreach (string move in moves.ToList())
                        if (move.Last() == '8')
                        {
                            moves.Remove(move);
                            promotionMoves.AddRange(new string[] { $"{move}q", $"{move}r", $"{move}b", $"{move}n" });
                        }
                    promotionMoves.AddRange(moves);
                    moves = promotionMoves;
                }
                foreach (string halfmove in moves)
                {
                    string move = piece.Position + halfmove;
                    forwardPlayPossible = true;
                    ChessBoard nextState = boardState.CloneWithMove(move);
                    List<string> nextMoveChain;
                    int score = AlphaBetaMin(nextState, alpha, beta, depthLeft - 1, maxDepth, out nextMoveChain);
                    if (score >= beta)
                        return beta;//beta cut-off
                    if (score > alpha)
                    {
                        alpha = score;
                        bestMoveChain.Clear();
                        bestMoveChain.Add(move);
                        bestMoveChain.AddRange(nextMoveChain);
                    }
                }
            }
            if (!forwardPlayPossible)
            {
                GameResult result = GameResult.Draw;
                if (boardState.CheckIfGameEnded(out result))
                {
                    if (result == GameResult.WhiteVictory)
                        alpha = 10000;
                    else if (result == GameResult.BlackVictory)
                        alpha = -10000;
                    else
                        alpha = 0;
                }
            }
            return alpha;
        }

        private int AlphaBetaMin(ChessBoard boardState, int alpha, int beta, int depthLeft, int maxDepth, out List<string> bestMoveChain)
        {
            bestMoveChain = new List<string>();
            if (StopSearch)
                return 0;
            if (depthLeft == 0)
                return EvaluatePosition(boardState);
            bool forwardPlayPossible = false;
            foreach (ChessPiece piece in boardState.Pieces.Values.Where(p => p.Color == boardState.CurrentPlayerColor).ToArray())
            {
                if (StopSearch)
                    return 0;
                List<string> moves = depthLeft > maxDepth - NumberOfValidLevels
                    ? piece.PossibleMoves(boardState.Pieces)
                    : piece.PseudoPossibleMoves(boardState.Pieces);
                if (piece.GetLetterAbbreviation() == "")
                {
                    List<string> promotionMoves = new List<string>();
                    foreach (string move in moves.ToList())
                        if (move.Last() == '1')
                        {
                            moves.Remove(move);
                            promotionMoves.AddRange(new string[] { $"{move}q", $"{move}r", $"{move}b", $"{move}n" });
                        }
                    promotionMoves.AddRange(moves);
                    moves = promotionMoves;
                }
                foreach (string halfmove in moves)
                {
                    string move = piece.Position + halfmove;
                    forwardPlayPossible = true;
                    ChessBoard nextState = boardState.CloneWithMove(move);
                    List<string> nextMoveChain;
                    int score = AlphaBetaMax(nextState, alpha, beta, depthLeft - 1, maxDepth, out nextMoveChain);
                    if (score <= alpha)
                        return alpha;//alpha cut-off
                    if (score < beta)
                    {
                        beta = score;
                        bestMoveChain.Clear();
                        bestMoveChain.Add(move);
                        bestMoveChain.AddRange(nextMoveChain);
                    }
                }
            }
            if (!forwardPlayPossible)
            {
                GameResult result = GameResult.Draw;
                if (boardState.CheckIfGameEnded(out result))
                {
                    if (result == GameResult.WhiteVictory)
                        beta = 10000;
                    else if (result == GameResult.BlackVictory)
                        beta = -10000;
                    else
                        beta = 0;
                }
            }
            return beta;
        }

        private int EvaluatePosition(ChessBoard boardState)
        {
            string[] simpleBoard = boardState.GetSimpleBoardNotation();
            int wminor = 0, bminor = 0, wrooks = 0, brooks = 0, wqueen = 0, bqueen = 0;
            int wkingpos = 0, bkingpos = 0;
            int whitePoints = 0, blackPoints = 0;
            for(int i = 0; i < simpleBoard.Length; ++i)
                if (!string.IsNullOrEmpty(simpleBoard[i]))
                {
                    switch (simpleBoard[i])
                    {
                        case "P":
                            {
                                whitePoints += PawnValue;
                                whitePoints += WhitePawnTable[i];
                                break;
                            }
                        case "p":
                            {
                                blackPoints += PawnValue;
                                blackPoints += BlackPawnTable[i];
                                break;
                            }
                        case "N":
                            {
                                ++wminor;
                                whitePoints += KnightValue;
                                whitePoints += WhiteKnightTable[i];
                                break;
                            }
                        case "n":
                            {
                                ++bminor;
                                blackPoints += KnightValue;
                                blackPoints += BlackKnightTable[i];
                                break;
                            }
                        case "B":
                            {
                                ++wminor;
                                whitePoints += BishopValue;
                                whitePoints += WhiteBishopTable[i];
                                break;
                            }
                        case "b":
                            {
                                ++bminor;
                                blackPoints += BishopValue;
                                blackPoints += BlackBishopTable[i];
                                break;
                            }
                        case "R":
                            {
                                ++wrooks;
                                whitePoints += RookValue;
                                whitePoints += WhiteRookTable[i];
                                break;
                            }
                        case "r":
                            {
                                ++brooks;
                                blackPoints += RookValue;
                                blackPoints += BlackRookTable[i];
                                break;
                            }
                        case "Q":
                            {
                                ++wqueen;
                                whitePoints += QueenValue;
                                whitePoints += WhiteQueenTable[i];
                                break;
                            }
                        case "q":
                            {
                                ++bqueen;
                                blackPoints += QueenValue;
                                blackPoints += BlackQueenTable[i];
                                break;
                            }
                        case "K":
                            {
                                wkingpos = i;
                                whitePoints += KingValue;
                                break;
                            }
                        case "k":
                            {
                                bkingpos = i;
                                blackPoints += KingValue;
                                break;
                            }
                    }
                }
            const int maxminor = 1;
            bool wlate = wqueen == 0 || wrooks == 0 && wminor <= maxminor;
            bool blate = bqueen == 0 || brooks == 0 && bminor <= maxminor;
            if(wlate && blate)//late-game
            {
                whitePoints += WhiteKingLateTable[wkingpos];
                blackPoints += BlackKingLateTable[bkingpos];
            }
            else //middle-game
            {
                whitePoints += WhiteKingMiddleTable[wkingpos];
                blackPoints += BlackKingMiddleTable[bkingpos];
            }

            return whitePoints - blackPoints;
        }

        #region evaluation constants

        /************************************
        **Valori pentru evaluarea materiala**
        ************************************/
        private const int PawnValue = 100;
        private const int KnightValue = 320;
        private const int BishopValue = 330;
        private const int RookValue = 500;
        private const int QueenValue = 900;
        private const int KingValue = 20000;

        /***********************************
        **Tabele pentru evaluarea pozitiei**
        ***********************************/
        //pawn
        private static readonly int[] WhitePawnTable =
        {
            0,  0,  0,  0,  0,  0,  0,  0,
            50, 50, 50, 50, 50, 50, 50, 50,
            10, 10, 20, 30, 30, 20, 10, 10,
            5,  5, 10, 25, 25, 10,  5,  5,
            0,  0,  0, 20, 20,  0,  0,  0,
            5, -5,-10,  0,  0,-10, -5,  5,
            5, 10, 10,-20,-20, 10, 10,  5,
            0,  0,  0,  0,  0,  0,  0,  0
        };
        private static readonly int[] BlackPawnTable =
        {
            0,  0,  0,  0,  0,  0,  0,  0,
            5, 10, 10,-20,-20, 10, 10,  5,
            5, -5,-10,  0,  0,-10, -5,  5,
            0,  0,  0, 20, 20,  0,  0,  0,
            5,  5, 10, 25, 25, 10,  5,  5,
            10, 10, 20, 30, 30, 20, 10, 10,
            50, 50, 50, 50, 50, 50, 50, 50,
            0,  0,  0,  0,  0,  0,  0,  0
        };

        //knight
        private static readonly int[] WhiteKnightTable =
        {
            -50,-40,-30,-30,-30,-30,-40,-50,
            -40,-20,  0,  0,  0,  0,-20,-40,
            -30,  0, 10, 15, 15, 10,  0,-30,
            -30,  5, 15, 20, 20, 15,  5,-30,
            -30,  0, 15, 20, 20, 15,  0,-30,
            -30,  5, 10, 15, 15, 10,  5,-30,
            -40,-20,  0,  5,  5,  0,-20,-40,
            -50,-40,-30,-30,-30,-30,-40,-50
        };
        private static readonly int[] BlackKnightTable =
        {
            -50,-40,-30,-30,-30,-30,-40,-50,
            -40,-20,  0,  5,  5,  0,-20,-40,
            -30,  5, 10, 15, 15, 10,  5,-30,
            -30,  0, 15, 20, 20, 15,  0,-30,
            -30,  5, 15, 20, 20, 15,  5,-30,
            -30,  0, 10, 15, 15, 10,  0,-30,
            -40,-20,  0,  0,  0,  0,-20,-40,
            -50,-40,-30,-30,-30,-30,-40,-50
        };

        //bishop
        private static readonly int[] WhiteBishopTable =
        {
            -20,-10,-10,-10,-10,-10,-10,-20,
            -10,  0,  0,  0,  0,  0,  0,-10,
            -10,  0,  5, 10, 10,  5,  0,-10,
            -10,  5,  5, 10, 10,  5,  5,-10,
            -10,  0, 10, 10, 10, 10,  0,-10,
            -10, 10, 10, 10, 10, 10, 10,-10,
            -10,  5,  0,  0,  0,  0,  5,-10,
            -20,-10,-10,-10,-10,-10,-10,-20
        };
        private static readonly int[] BlackBishopTable =
        {
            -20,-10,-10,-10,-10,-10,-10,-20,
            -10,  5,  0,  0,  0,  0,  5,-10,
            -10, 10, 10, 10, 10, 10, 10,-10,
            -10,  0, 10, 10, 10, 10,  0,-10,
            -10,  5,  5, 10, 10,  5,  5,-10,
            -10,  0,  5, 10, 10,  5,  0,-10,
            -10,  0,  0,  0,  0,  0,  0,-10,
            -20,-10,-10,-10,-10,-10,-10,-20
        };

        //rook
        private static readonly int[] WhiteRookTable =
        {
            0,  0,  0,  0,  0,  0,  0,  0,
            5, 10, 10, 10, 10, 10, 10,  5,
           -5,  0,  0,  0,  0,  0,  0, -5,
           -5,  0,  0,  0,  0,  0,  0, -5,
           -5,  0,  0,  0,  0,  0,  0, -5,
           -5,  0,  0,  0,  0,  0,  0, -5,
           -5,  0,  0,  0,  0,  0,  0, -5,
            0,  0,  0,  5,  5,  0,  0,  0
        };
        private static readonly int[] BlackRookTable =
        {
            0,  0,  0,  5,  5,  0,  0,  0,
           -5,  0,  0,  0,  0,  0,  0, -5,
           -5,  0,  0,  0,  0,  0,  0, -5,
           -5,  0,  0,  0,  0,  0,  0, -5,
           -5,  0,  0,  0,  0,  0,  0, -5,
           -5,  0,  0,  0,  0,  0,  0, -5,
            5, 10, 10, 10, 10, 10, 10,  5,
            0,  0,  0,  0,  0,  0,  0,  0
        };

        //queen
        private static readonly int[] WhiteQueenTable =
        {
            -20,-10,-10, -5, -5,-10,-10,-20,
            -10,  0,  0,  0,  0,  0,  0,-10,
            -10,  0,  5,  5,  5,  5,  0,-10,
             -5,  0,  5,  5,  5,  5,  0, -5,
              0,  0,  5,  5,  5,  5,  0, -5,
            -10,  5,  5,  5,  5,  5,  0,-10,
            -10,  0,  5,  0,  0,  0,  0,-10,
            -20,-10,-10, -5, -5,-10,-10,-20
        };
        private static readonly int[] BlackQueenTable =
        {
            -20,-10,-10, -5, -5,-10,-10,-20,
            -10,  0,  5,  0,  0,  0,  0,-10,
            -10,  5,  5,  5,  5,  5,  0,-10,
              0,  0,  5,  5,  5,  5,  0, -5,
             -5,  0,  5,  5,  5,  5,  0, -5,
            -10,  0,  5,  5,  5,  5,  0,-10,
            -10,  0,  0,  0,  0,  0,  0,-10,
            -20,-10,-10, -5, -5,-10,-10,-20
        };

        //king-middle
        private static readonly int[] WhiteKingMiddleTable =
        {
            -30,-40,-40,-50,-50,-40,-40,-30,
            -30,-40,-40,-50,-50,-40,-40,-30,
            -30,-40,-40,-50,-50,-40,-40,-30,
            -30,-40,-40,-50,-50,-40,-40,-30,
            -20,-30,-30,-40,-40,-30,-30,-20,
            -10,-20,-20,-20,-20,-20,-20,-10,
             20, 20,  0,  0,  0,  0, 20, 20,
             20, 30, 10,  0,  0, 10, 30, 20
        };
        private static readonly int[] BlackKingMiddleTable =
        {
             20, 30, 10,  0,  0, 10, 30, 20,
             20, 20,  0,  0,  0,  0, 20, 20,
            -10,-20,-20,-20,-20,-20,-20,-10,
            -20,-30,-30,-40,-40,-30,-30,-20,
            -30,-40,-40,-50,-50,-40,-40,-30,
            -30,-40,-40,-50,-50,-40,-40,-30,
            -30,-40,-40,-50,-50,-40,-40,-30,
            -30,-40,-40,-50,-50,-40,-40,-30
        };

        //king-late
        private static readonly int[] WhiteKingLateTable =
        {
            -50,-40,-30,-20,-20,-30,-40,-50,
            -30,-20,-10,  0,  0,-10,-20,-30,
            -30,-10, 20, 30, 30, 20,-10,-30,
            -30,-10, 30, 40, 40, 30,-10,-30,
            -30,-10, 30, 40, 40, 30,-10,-30,
            -30,-10, 20, 30, 30, 20,-10,-30,
            -30,-30,  0,  0,  0,  0,-30,-30,
            -50,-30,-30,-30,-30,-30,-30,-50
        };
        private static readonly int[] BlackKingLateTable =
        {
            -50,-30,-30,-30,-30,-30,-30,-50,
            -30,-30,  0,  0,  0,  0,-30,-30,
            -30,-10, 20, 30, 30, 20,-10,-30,
            -30,-10, 30, 40, 40, 30,-10,-30,
            -30,-10, 30, 40, 40, 30,-10,-30,
            -30,-10, 20, 30, 30, 20,-10,-30,
            -30,-20,-10,  0,  0,-10,-20,-30,
            -50,-40,-30,-20,-20,-30,-40,-50
        };
        #endregion
    }
}
