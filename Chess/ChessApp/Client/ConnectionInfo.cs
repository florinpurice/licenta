﻿using ChessWcfInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessApp.Client
{
    public static class ConnectionInfo
    {
        public static ChessClientCallback Callback;
        public static IChessService ChessServiceProxy;
        public static bool IsRegistered;
        public static string PlayerName;
    }
}
