﻿using ChessWcfInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessApp.Client
{
    public delegate void ChessClientEventHandler(ChessClientEventArgs args);

    public class ChessClientEventArgs
    {
        public int GameId { get; set; }
        public string Move { get; set; }
        public GameInfo GameInfo { get; set; }
        public string Message { get; set; }
        public string OpponentName { get; set; }
    }
}
