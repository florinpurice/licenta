﻿using ChessWcfInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ChessApp.Client
{
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    public class ChessClientCallback : IChessClientCallback
    {
        public event ChessClientEventHandler GameStart;
        public event ChessClientEventHandler OpponentLeftGame;
        public event ChessClientEventHandler OpponentMove;
        public event ChessClientEventHandler MessageReceived;


        public void GameStarted(GameInfo gameInfo)
        {
            GameStart?.Invoke(new ChessClientEventArgs { GameId = gameInfo.Id, GameInfo = gameInfo });
        }

        public void OpponentLeft(int gameId)
        {
            OpponentLeftGame?.Invoke(new ChessClientEventArgs { GameId = gameId });
        }

        public void OpponentMoved(int gameId, string move)
        {
            OpponentMove?.Invoke(new ChessClientEventArgs { GameId = gameId, Move = move });
        }

        public void SendMessage(int gameId, string from, string message)
        {
            MessageReceived?.Invoke(new ChessClientEventArgs { GameId = gameId, OpponentName = from, Message = message });
        }
    }
}
