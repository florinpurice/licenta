﻿using ChessCore;
using Microsoft.Win32;
using SharpGL;
using SharpGL.SceneGraph;
using SharpGL.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPFLocalizeExtension.Engine;
using System.ComponentModel;
using System.Threading;

namespace ChessApp.Pages
{
    /// <summary>
    /// Interaction logic for OfflineGame.xaml
    /// </summary>
    public partial class OfflineGame : Page
    {
        private ChessBoard _chessBoard;
        private ChessCore.Scene _scene;

        public OfflineGame()
        {
            InitializeComponent();

            _chessBoard = chessDisplay.Board;
            _scene = chessDisplay.Scene;
            chessDisplay.PlayerMoved += chessDisplay_PlayerMoved;
            chessDisplay.GameEnded += chessDisplay_GameEnded;
        }
        
        public static readonly DependencyProperty AutoFlipEnabledProperty = DependencyProperty.Register("AutoFlipEnabled", typeof(bool), typeof(OfflineGame));
        public bool AutoFlipEnabled
        {
            get { return (bool)GetValue(AutoFlipEnabledProperty); }
            set { SetValue(AutoFlipEnabledProperty, value); }
        }

        private void chessDisplay_GameEnded(GameResult result, string details)
        {
            Task.Run(() =>
            {
                Thread.Sleep(1000);
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    string bigText;
                    switch (result)
                    {
                        case GameResult.WhiteVictory:
                            {
                                bigText = Utils.GetLocalizedString("WhiteVictory", "Strings");
                                break;
                            }
                        case GameResult.BlackVictory:
                            {
                                bigText = Utils.GetLocalizedString("BlackVictory", "Strings");
                                break;
                            }
                        default:
                            {
                                bigText = Utils.GetLocalizedString("Draw", "Strings");
                                break;
                            }
                    }
                    popupBig.Text = bigText;
                    popupSmall.Text = Utils.GetLocalizedString(details, "Strings");
                    popupMessage.Visibility = Visibility.Visible;
                }));
            });
        }

        private void chessDisplay_PlayerMoved(string move)
        {
            if (AutoFlipEnabled)
                FlipBoard();
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            if (!MainWindow.Pages.ContainsKey("mainmenu"))
                MainWindow.Pages["mainmenu"] = new MainMenu();
            MainWindow.Frame.Navigate(MainWindow.Pages["mainmenu"]);
        }

        public void newGameButton_Click(object sender, RoutedEventArgs e)
        {
            Windows.GameSetupWindow gsw = new Windows.GameSetupWindow();
            if (gsw.ShowDialog().GetValueOrDefault())
            {
                chessDisplay.WhitePlayerName = "Player1";
                chessDisplay.BlackPlayerName = "Player2";
                chessDisplay.NewGameWithClock((int)gsw.WTime, (int)gsw.BTime, (int)gsw.WInc, (int)gsw.BInc, false);
                demoTextBox.Visibility = Visibility.Hidden;
                chessDisplay.Visibility = Visibility.Visible;
            }
            popupMessage.Visibility = Visibility.Hidden;
        }

        private void closePopupButton_Click(object sender, RoutedEventArgs e)
        {
            popupMessage.Visibility = Visibility.Hidden;
        }

        private void flipBoardButton_Click(object sender, RoutedEventArgs e)
        {
            FlipBoard();
        }

        private void FlipBoard()
        {
            _scene.PlayerPerspectiveColor = _scene.PlayerPerspectiveColor == PieceColor.White
                ? PieceColor.Black
                : PieceColor.White;
        }
    }
}
