﻿using ChessApp.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ChessApp.Pages
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Page
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void playOnlineButton_Click(object sender, RoutedEventArgs e)
        {
            if(ConnectionInfo.ChessServiceProxy != null && ConnectionInfo.IsRegistered)
            {
                if (!MainWindow.Pages.ContainsKey("onlineloby"))
                    MainWindow.Pages["onlineloby"] = new OnlineLoby();
                OnlineLoby page = MainWindow.Pages["onlineloby"] as OnlineLoby;
                page.RefreshPage();
                MainWindow.Frame.Navigate(page);
            }
            else
            {
                if (!MainWindow.Pages.ContainsKey("preloby"))
                    MainWindow.Pages["preloby"] = new PreLobyPage();
                PreLobyPage page = MainWindow.Pages["preloby"] as PreLobyPage;
                page.RefreshPage();
                MainWindow.Frame.Navigate(page);
            }
        }

        private void playFriendOfflineButton_Click(object sender, RoutedEventArgs e)
        {
            if (!MainWindow.Pages.ContainsKey("offlinegame"))
                MainWindow.Pages["offlinegame"] = new OfflineGame();
            MainWindow.Frame.Navigate(MainWindow.Pages["offlinegame"]);
        }
        private void playAgainstComputerButton_Click(object sender, RoutedEventArgs e)
        {
            if (!MainWindow.Pages.ContainsKey("computergame"))
            {
                ComputerGame page = new ComputerGame();
                MainWindow.Pages["computergame"] = page;
                MainWindow.Frame.Navigate(page);
                if (page.TryLoadPredefined())
                    page.newGameButton_Click(null, null);
            }
            else
                MainWindow.Frame.Navigate(MainWindow.Pages["computergame"]);
        }

        private void trainingGameButton_Click(object sender, RoutedEventArgs e)
        {
            if (!MainWindow.Pages.ContainsKey("traininggame"))
                MainWindow.Pages["traininggame"] = new TrainingGame();
            MainWindow.Frame.Navigate(MainWindow.Pages["traininggame"]);
        }

        private void romanianButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.ChangeCulture("ro-RO");
        }

        private void englishButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.ChangeCulture("en");
        }
    }
}
