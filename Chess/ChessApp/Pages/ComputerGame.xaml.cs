﻿using ChessApp.AI;
using ChessCore;
using Microsoft.Win32;
using SharpGL;
using SharpGL.SceneGraph;
using SharpGL.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPFLocalizeExtension.Engine;
using System.ComponentModel;
using System.Threading;
using ChessApp.Windows;
using System.Xml.Linq;

namespace ChessApp.Pages
{
    /// <summary>
    /// Interaction logic for ComputerGame.xaml
    /// </summary>
    public partial class ComputerGame : Page
    {
        private ChessBoard _chessBoard;
        private ChessCore.Scene _scene;
        private UciWrapper _chessEngine;

        private int wtime = 300000, btime = 300000, winc = 5000, binc = 5000;

        public ComputerGame()
        {
            InitializeComponent();

            _chessBoard = chessDisplay.Board;
            _scene = chessDisplay.Scene;
            chessDisplay.PlayerMoved += chess_Display_PlayerMoved;
            chessDisplay.GameEnded += chessDisplay_GameEnded;
            Application.Current.MainWindow.Closing += MainWindow_Closing;
        }

        public bool TryLoadPredefined()
        {
            List<string> predefEnginePaths = new List<string>();
            try
            {
                XDocument doc = XDocument.Load("Engines/Predefined.xml");
                foreach (XElement engine in doc.Root.Elements("engine"))
                {
                    string file = engine.Attribute("file").Value;
                    predefEnginePaths.Add(file);
                }
            }
            catch { }
            if (predefEnginePaths.Count == 0)
                return false;
            try
            {
                UciWrapper newEngine = new UciWrapper("Engines/" + predefEnginePaths[predefEnginePaths.Count / 2]);
                //Engine-player interaction
                newEngine.EngineMoved += uciWrapper_EngineMoved;
                newEngine.EngineNameRecieved += uciWrapper_EngineNameRecieved;
                _chessEngine?.Dispose();
                _chessEngine = newEngine;
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void chessDisplay_GameEnded(GameResult result, string details)
        {
            _chessEngine?.SendStopCommand();
            Task.Run(() =>
            {
                Thread.Sleep(1000);
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    string bigText;
                    switch (result)
                    {
                        case GameResult.WhiteVictory:
                            {
                                bigText = Utils.GetLocalizedString("WhiteVictory", "Strings");
                                break;
                            }
                        case GameResult.BlackVictory:
                            {
                                bigText = Utils.GetLocalizedString("BlackVictory", "Strings");
                                break;
                            }
                        default:
                            {
                                bigText = Utils.GetLocalizedString("Draw", "Strings");
                                break;
                            }
                    }
                    popupBig.Text = bigText;
                    popupSmall.Text = Utils.GetLocalizedString(details, "Strings");
                    popupMessage.Visibility = Visibility.Visible;
                }));
            });
        }

        private void uciWrapper_EngineMoved(string move)
        {
            chessDisplay?.EnemyMove(move);
        }

        private void chess_Display_PlayerMoved(string move)
        {
            Clock clock = chessDisplay.Clock;
            _chessEngine?.PlayerMove(move);
            _chessEngine?.ChangeClockTimes(clock.WhiteTime, clock.BlackTime, clock.WhiteIncrement, clock.BlackIncrement);
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            if (!MainWindow.Pages.ContainsKey("mainmenu"))
                MainWindow.Pages["mainmenu"] = new MainMenu();
            MainWindow.Frame.Navigate(MainWindow.Pages["mainmenu"]);
        }

        public void newGameButton_Click(object sender, RoutedEventArgs e)
        {
            if (_chessEngine == null)
            {
                string message = Utils.GetLocalizedString("NoEngineLoaded", "Strings");
                popupBig.Text = string.Empty;
                popupSmall.Text = message;
                popupMessage.Visibility = Visibility.Visible;
            }
            else
            {
                Windows.GameSetupWindow gsw = new Windows.GameSetupWindow();
                if (gsw.ShowDialog().GetValueOrDefault())
                {
                    string color = gsw.PlayerColor;
                    if (color == "random")
                        color = Utils.RandomGenerator.Next() % 2 == 0 ? "white" : "black";
                    if (color == "white")
                    {
                        chessDisplay.WhitePlayerName = "Player";
                        chessDisplay.BlackPlayerName = _chessEngine.EngineName;
                        chessDisplay.NewGameWithClock((int)gsw.WTime, (int)gsw.BTime, (int)gsw.WInc, (int)gsw.BInc, true, PieceColor.White);
                    }
                    else
                    {
                        chessDisplay.WhitePlayerName = _chessEngine.EngineName;
                        chessDisplay.BlackPlayerName = "Player";
                        chessDisplay.NewGameWithClock((int)gsw.WTime, (int)gsw.BTime, (int)gsw.WInc, (int)gsw.BInc, true, PieceColor.Black);
                    }
                    _chessEngine.NewGame((int)gsw.WTime, (int)gsw.BTime, (int)gsw.WInc, (int)gsw.BInc);
                    if (chessDisplay.PlayerRestrictionColor != _chessBoard.CurrentPlayerColor)
                        _chessEngine.DoFirstMove();

                    demoTextBox.Visibility = Visibility.Hidden;
                    chessDisplay.Visibility = Visibility.Visible;
                }
                popupMessage.Visibility = Visibility.Hidden;
            }
        }

        private void closePopupButton_Click(object sender, RoutedEventArgs e)
        {
            popupMessage.Visibility = Visibility.Hidden;
        }

        private void endGameButton_Click(object sender, RoutedEventArgs e)
        {
            chessDisplay.StopGame(GameResult.Draw, null);
        }

        private void loadEngineButton_Click(object sender, RoutedEventArgs e)
        {
            int wtime = this.wtime, btime = this.btime, winc = this.winc, binc = this.binc;
            if (_chessEngine != null)
            {
                wtime = _chessEngine.WhiteTimeLeft;
                btime = _chessEngine.BlackTimeLeft;
                winc = _chessEngine.WhiteIncrement;
                binc = _chessEngine.BlackIncrement;
            }
            LoadEngineWindow lew = new LoadEngineWindow();
            if (lew.ShowDialog().GetValueOrDefault())
                try
                {
                    UciWrapper newEngine = new UciWrapper(lew.FilePath);
                    //Engine-player interaction
                    newEngine.EngineMoved += uciWrapper_EngineMoved;
                    newEngine.EngineNameRecieved += uciWrapper_EngineNameRecieved;
                    _chessEngine?.Dispose();
                    _chessEngine = newEngine;
                    if (chessDisplay.GameInProgress)
                    {
                        _chessEngine.NewGame(wtime, btime, winc, binc);
                        _chessEngine.MoveList.AddRange(_chessBoard.PacnMoveList);
                        if (chessDisplay.PlayerRestrictionColor != _chessBoard.CurrentPlayerColor)
                            _chessEngine.DoFirstMove();
                    }
                    popupMessage.Visibility = Visibility.Hidden;
                }
                catch
                {
                    string message = Utils.GetLocalizedString("EngineLoadError", "Strings");
                    popupBig.Text = string.Empty;
                    popupSmall.Text = message;
                    popupMessage.Visibility = Visibility.Visible;
                }
        }

        private void uciWrapper_EngineNameRecieved(string name)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                engineNameTextBlock.Text = name;
                engineNotLoadedTextBlock.Visibility = Visibility.Collapsed;
                engineLoadedLabel.Visibility = Visibility.Visible;
                if (chessDisplay.GameInProgress)
                    if (chessDisplay.PlayerRestrictionColor == PieceColor.White)
                        chessDisplay.BlackPlayerName = name;
                    else
                        chessDisplay.WhitePlayerName = name;
            }));
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            _chessEngine?.Dispose();
        }
    }
}
