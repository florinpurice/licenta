﻿using ChessApp.Client;
using ChessCore;
using ChessWcfInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ChessApp.Pages
{
    /// <summary>
    /// Interaction logic for OnlineGame.xaml
    /// </summary>
    public partial class OnlineGame : Page
    {
        private IChessService _proxy;
        private ChessClientCallback _callback;
        private GameInfo _gameInfo;
        private ChessBoard _chessBoard;
        private ChessCore.Scene _scene;

        public OnlineGame()
        {
            InitializeComponent();
            _chessBoard = chessDisplay.Board;
            _scene = chessDisplay.Scene;
            chessDisplay.PlayerMoved += chessDisplay_PlayerMoved;
            chessDisplay.GameEnded += chessDisplay_GameEnded;

            _proxy = ConnectionInfo.ChessServiceProxy;
            ConnectCallbackHandlers();
        }

        public void NewGame(GameInfo gi)
        {
            if (_proxy != ConnectionInfo.ChessServiceProxy)
            {
                _proxy = ConnectionInfo.ChessServiceProxy;
                ConnectCallbackHandlers();
            }
            popupMessage.Visibility = Visibility.Hidden;
            chatTextBox.Text = string.Empty;
            _gameInfo = gi;
            PieceColor hostColor = gi.HostColor == "white" ? PieceColor.White : PieceColor.Black;
            PieceColor guestColor = hostColor == PieceColor.White ? PieceColor.Black : PieceColor.White;
            PieceColor playAsColor = gi.IsOwned ? hostColor : guestColor;
            string whitePlayerName = gi.HostName, blackPlayerName = gi.GuestName;
            if(hostColor != PieceColor.White)
            {
                whitePlayerName = gi.GuestName;
                blackPlayerName = gi.HostName;
            }
            chessDisplay.WhitePlayerName = whitePlayerName;
            chessDisplay.BlackPlayerName = blackPlayerName;
            chessDisplay.NewGameWithClock((int)gi.Time * 1000, (int)gi.Time * 1000, (int)gi.Increment * 1000, (int)gi.Increment * 1000, true, playAsColor);
        }

        private void leaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (!MainWindow.Pages.ContainsKey("onlineloby"))
                MainWindow.Pages["onlineloby"] = new OnlineLoby();
            OnlineLoby page = MainWindow.Pages["onlineloby"] as OnlineLoby;
            page.RefreshPage();
            MainWindow.Frame.Navigate(page);
            _proxy.LeaveGame(_gameInfo.Id);
            _gameInfo = null;
        }

        private void ConnectCallbackHandlers()
        {
            _callback = ConnectionInfo.Callback;
            _callback.OpponentLeftGame += Callback_OpponentLeftGame;
            _callback.OpponentMove += Callback_OpponentMove;
            _callback.MessageReceived += Callback_MessageReceived;
        }

        private void chessDisplay_PlayerMoved(string move)
        {
            _proxy.MakeMove(_gameInfo.Id, move);
        }

        private void Callback_OpponentMove(ChessClientEventArgs args)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (_gameInfo != null && _gameInfo.Id == args.GameId)
                    chessDisplay.EnemyMove(args.Move);
            }));
        }

        private void Callback_OpponentLeftGame(ChessClientEventArgs args)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (_gameInfo != null && _gameInfo.Id == args.GameId)
                {
                    chatTextBox.Text += $"<{Utils.GetLocalizedString("opponentleft", "Strings")}>";
                    if (chessDisplay.GameInProgress)
                    {
                        GameResult result = GameResult.BlackVictory;
                        if (chessDisplay.PlayerRestrictionColor == PieceColor.White)
                            result = GameResult.WhiteVictory;
                        chessDisplay.StopGame(result, "opponentleft");
                    }
                }
            }));
        }

        private void Callback_MessageReceived(ChessClientEventArgs args)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                chatTextBox.Text += $"{args.OpponentName}: {args.Message}\n";
            }));
        }

        private void sendButton_Click(object sender, RoutedEventArgs e)
        {
            if(_gameInfo != null)
            {
                string message = messageTb.Text;
                messageTb.Text = string.Empty;
                string name = _gameInfo.IsOwned ? _gameInfo.HostName : _gameInfo.GuestName;
                chatTextBox.Text += $"{name}: {message}\n";
                _proxy.SendMessage(_gameInfo.Id, message);
            }

        }
        private void chessDisplay_GameEnded(GameResult result, string details)
        {
            Task.Run(() =>
            {
                Thread.Sleep(1000);
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    string bigText;
                    switch (result)
                    {
                        case GameResult.WhiteVictory:
                            {
                                bigText = Utils.GetLocalizedString("WhiteVictory", "Strings");
                                break;
                            }
                        case GameResult.BlackVictory:
                            {
                                bigText = Utils.GetLocalizedString("BlackVictory", "Strings");
                                break;
                            }
                        default:
                            {
                                bigText = Utils.GetLocalizedString("Draw", "Strings");
                                break;
                            }
                    }
                    popupBig.Text = bigText;
                    popupSmall.Text = Utils.GetLocalizedString(details, "Strings");
                    popupMessage.Visibility = Visibility.Visible;
                }));
            });
        }

        private void closePopupButton_Click(object sender, RoutedEventArgs e)
        {
            popupMessage.Visibility = Visibility.Hidden;
        }

        private void messageTb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
                sendButton_Click(null, null);
        }
    }
}
