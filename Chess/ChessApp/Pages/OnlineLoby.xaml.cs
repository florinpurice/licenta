﻿using ChessApp.Client;
using ChessApp.Windows;
using ChessWcfInterfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ChessApp.Pages
{
    /// <summary>
    /// Interaction logic for OnlineLoby.xaml
    /// </summary>
    public partial class OnlineLoby : Page
    {
        private const int RefreshTimeout = 5000;
        private static CancellationTokenSource _refreshThreadCts;
        private IChessService _proxy;
        private ChessClientCallback _callback;

        public OnlineLoby()
        {
            InitializeComponent();
            dataGrid.ItemsSource = GameList;
            _proxy = ConnectionInfo.ChessServiceProxy;
            ConnectCallbackHandlers();
            StartRefreshThread();
        }

        private void StartRefreshThread()
        {
            _refreshThreadCts?.Cancel();
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken token = cts.Token;
            Task.Run(() =>
            {
                while (!token.IsCancellationRequested)
                {
                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        if (MainWindow.IsPageCurrent("onlineloby"))
                            RefreshPage();
                    }));
                    Thread.Sleep(RefreshTimeout);
                }
            }, token);
            _refreshThreadCts = cts;
        }

        public ObservableCollection<GameInfo> GameList { get; set; } = new ObservableCollection<GameInfo>();

        public void RefreshPage()
        {
            if(_proxy != ConnectionInfo.ChessServiceProxy)
            {
                _proxy = ConnectionInfo.ChessServiceProxy;
                ConnectCallbackHandlers();
            }
            nameLabel.Content = Utils.GetLocalizedString("ConnectedAs", "Strings") + ConnectionInfo.PlayerName;
            List<GameInfo> list = _proxy.GetGameList();
            GameList.Clear();
            foreach (GameInfo gi in list)
                GameList.Add(gi);
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            if (!MainWindow.Pages.ContainsKey("mainmenu"))
                MainWindow.Pages["mainmenu"] = new MainMenu();
            MainWindow.Frame.Navigate(MainWindow.Pages["mainmenu"]);
        }

        private void ConnectCallbackHandlers()
        {
            _callback = ConnectionInfo.Callback;
            _callback.GameStart += Callback_GameStart;
        }

        private void joinGameButton_Click(object sender, RoutedEventArgs e)
        {
            GameInfo gi = ((FrameworkElement)sender).DataContext as GameInfo;
            string password = null;
            if (gi.IsPasswordProtected)
            {
                JoinWithPasswordWindow jwpw = new JoinWithPasswordWindow();
                if (jwpw.ShowDialog().GetValueOrDefault())
                {
                    password = jwpw.Password;
                }
            }
            _proxy.JoinGame(gi.Id, password);
        }

        private void removeGameButton_Click(object sender, RoutedEventArgs e)
        {
            GameInfo gi = ((FrameworkElement)sender).DataContext as GameInfo;
            _proxy.RemoveGame(gi.Id);
            RefreshPage();
        }

        private void Callback_GameStart(ChessClientEventArgs args)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (!MainWindow.Pages.ContainsKey("onlinegame"))
                    MainWindow.Pages["onlinegame"] = new OnlineGame();
                OnlineGame page = MainWindow.Pages["onlinegame"] as OnlineGame;
                page.NewGame(args.GameInfo);
                MainWindow.Frame.Navigate(page);
            }));
        }

        private void createGameButton_Click(object sender, RoutedEventArgs e)
        {
            
            Windows.OnlineGameSetupWindow ogsw = new Windows.OnlineGameSetupWindow();
            if (ogsw.ShowDialog().GetValueOrDefault())
            {
                GameInfo gi = new GameInfo
                {
                    GameName = ogsw.GameName,
                    HostColor = ogsw.PlayerColor,
                    Time = ogsw.Time,
                    Increment = ogsw.Increment,
                    IsPasswordProtected = ogsw.IsPasswordRestricted
                };
                string password = null;
                if (ogsw.IsPasswordRestricted)
                    password = ogsw.Password;
                _proxy.CreateGame(gi, password);
            }
            RefreshPage();
        }
    }
}
