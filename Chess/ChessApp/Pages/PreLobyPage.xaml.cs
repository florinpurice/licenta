﻿using ChessApp.Client;
using ChessWcfInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ChessApp.Pages
{
    /// <summary>
    /// Interaction logic for PreLobyPage.xaml
    /// </summary>
    public partial class PreLobyPage : Page
    {
        public PreLobyPage()
        {
            InitializeComponent();
        }

        public void RefreshPage()
        {
            errorTextBlock.Text = string.Empty;
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            if (!MainWindow.Pages.ContainsKey("mainmenu"))
                MainWindow.Pages["mainmenu"] = new MainMenu();
            MainWindow.Frame.Navigate(MainWindow.Pages["mainmenu"]);
        }

        private void connectButton_Click(object sender, RoutedEventArgs e)
        {
            errorTextBlock.Text = string.Empty;
            string name = playerNameTb.Text;
            if (string.IsNullOrWhiteSpace(name))
            {
                playerNameTb.Text = string.Empty;
                errorTextBlock.Text = Utils.GetLocalizedString("InvalidNameError", "Strings");
            }
            else
            {
                if (ConnectionInfo.ChessServiceProxy == null)
                    ConnectToService();
                if(ConnectionInfo.ChessServiceProxy != null)
                {
                    if (ConnectionInfo.ChessServiceProxy.Connect(name))
                    {
                        ConnectionInfo.PlayerName = name;
                        ConnectionInfo.IsRegistered = true;
                        if (!MainWindow.Pages.ContainsKey("onlineloby"))
                            MainWindow.Pages["onlineloby"] = new OnlineLoby();
                        OnlineLoby page = MainWindow.Pages["onlineloby"] as OnlineLoby;
                        page.RefreshPage();
                        MainWindow.Frame.Navigate(page);
                    }
                    else
                    {
                        ConnectionInfo.PlayerName = null;
                        ConnectionInfo.IsRegistered = false;
                        playerNameTb.Text = string.Empty;
                        errorTextBlock.Text = Utils.GetLocalizedString("NameUnavailableError", "Strings");
                    }
                }
            }
        }

        private void ConnectToService()
        {
            ConnectionInfo.Callback = new ChessClientCallback();
            DuplexChannelFactory<IChessService> dcf = new DuplexChannelFactory<IChessService>(ConnectionInfo.Callback, "ChessServiceEndpoint");
            ConnectionInfo.ChessServiceProxy = dcf.CreateChannel();
            try
            {
                ICommunicationObject comObj = ConnectionInfo.ChessServiceProxy as ICommunicationObject;
                comObj.Closed += new EventHandler((s, e) => HandleClosedConnection());
                comObj.Faulted += new EventHandler((s, e) => HandleClosedConnection());
                comObj.Open();
            }
            catch
            {
                //TO-DO localizare
                ConnectionInfo.ChessServiceProxy = null;
                errorTextBlock.Text = Utils.GetLocalizedString("ServerDownError", "Strings");
            }
        }

        private void HandleClosedConnection()
        {
            ConnectionInfo.ChessServiceProxy = null;
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (MainWindow.IsPageCurrent("onlineloby") || MainWindow.IsPageCurrent("onlinegame"))
                {
                    if (!MainWindow.Pages.ContainsKey("preloby"))
                        MainWindow.Pages["preloby"] = new PreLobyPage();
                    PreLobyPage page = MainWindow.Pages["preloby"] as PreLobyPage;
                    errorTextBlock.Text = Utils.GetLocalizedString("ServerDownError", "Strings");
                    MainWindow.Frame.Navigate(page);
                }
            }));
        }

        private void playerNameTb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
                connectButton_Click(null, null);
        }
    }
}
