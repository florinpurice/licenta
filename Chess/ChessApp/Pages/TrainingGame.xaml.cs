﻿using ChessCore;
using Microsoft.Win32;
using SharpGL;
using SharpGL.SceneGraph;
using SharpGL.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPFLocalizeExtension.Engine;
using System.ComponentModel;
using System.Threading;
using ChessApp.AI;

namespace ChessApp.Pages
{
    /// <summary>
    /// Interaction logic for Testing_temp.xaml
    /// </summary>
    public partial class TrainingGame : Page
    {
        private ChessBoard _chessBoard;
        private ChessCore.Scene _scene;
        private SimpleEngine _chessEngine;
        private Dictionary<string, MoveChain> _bestLinesOfPlay = new Dictionary<string, MoveChain>();
        private readonly List<SimpleEngine> _runningSearches = new List<SimpleEngine>();
        private object _rsLock = new object();
        private object _blopLock = new object();
        private ChessCore.Scene _auxScene;
        private bool _canSugest = false;
        private float _maxScore, _minScore;
        private CancellationTokenSource _sugestionTaskCts;
        private CancellationTokenSource _evaluationTaskCts;

        public TrainingGame()
        {
            InitializeComponent();

            _chessBoard = chessDisplay.Board;
            _scene = chessDisplay.Scene;
            chessDisplay.PlayerMoved += chess_Display_PlayerMoved;
            chessDisplay.ControlMove += chessDisplay_ControlMove;
            chessDisplay.GameEnded += chessDisplay_GameEnded;
            Application.Current.MainWindow.Closing += MainWindow_Closing;

            _chessEngine = new SimpleEngine();
            _chessEngine.EngineMoved += chessEngine_EngineMoved;

            _auxScene = new ChessCore.Scene(_chessBoard.Clone());

            chessDisplay.OpenGLControl.Resized += CorrectDimensions;
            openGLControl.Resized += CorrectDimensions;
        }

        private void chessDisplay_ControlMove(string move)
        {
            MoveSugestion(move);
        }

        private void chessDisplay_GameEnded(GameResult result, string details)
        {
            StopAll();
            Task.Run(() =>
            {
                Thread.Sleep(1000);
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    string bigText;
                    switch (result)
                    {
                        case GameResult.WhiteVictory:
                            {
                                bigText = Utils.GetLocalizedString("WhiteVictory", "Strings");
                                break;
                            }
                        case GameResult.BlackVictory:
                            {
                                bigText = Utils.GetLocalizedString("BlackVictory", "Strings");
                                break;
                            }
                        default:
                            {
                                bigText = Utils.GetLocalizedString("Draw", "Strings");
                                break;
                            }
                    }
                    popupBig.Text = bigText;
                    popupSmall.Text = details == null ? "" : Utils.GetLocalizedString(details, "Strings");
                    popupMessage.Visibility = Visibility.Visible;
                }));
            });
        }

        private void chessEngine_EngineMoved(string move)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                sugestionLabel.Visibility = Visibility.Visible;
                evaluationPanel.Visibility = Visibility.Hidden;
                betterMoveLabel.Visibility = Visibility.Hidden;
            }));
            if (chessDisplay.GameInProgress)
            {
                chessDisplay?.EnemyMove(move);
                PrepareMoveEvaluations(_chessBoard);
            }
        }

        private void chess_Display_PlayerMoved(string move)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                sugestionLabel.Visibility = Visibility.Hidden;
                evaluationPanel.Visibility = Visibility.Hidden;
                CheckBetterMove(move);
                betterMoveLabel.Visibility = Visibility.Visible;
                _canSugest = false;
            }));
            _evaluationTaskCts?.Cancel();
            ClearRunningSearches();
            _chessEngine?.PlayerMove(move);
        }

        private void PrepareMoveEvaluations(ChessBoard board)
        {
            _canSugest = false;
            lock (_blopLock)
            {
                _bestLinesOfPlay.Clear();
            }
            _evaluationTaskCts?.Cancel();
            ClearRunningSearches();
            _evaluationTaskCts = new CancellationTokenSource();
            CancellationToken token = _evaluationTaskCts.Token;
            Task.Run(() =>
            {
                int parallelThreadCount = Environment.ProcessorCount - 1;
                if (parallelThreadCount < 1)
                    parallelThreadCount = 1;
                List<string> possibleMoves = new List<string>();
                foreach (ChessPiece piece in board.Pieces.Values.Where(p => p.Color == board.CurrentPlayerColor).ToArray())
                    foreach (string halfmove in piece.PossibleMoves(board.Pieces))
                        possibleMoves.Add($"{piece.Position}{halfmove}");
                int depth = 1;
                while (!token.IsCancellationRequested)
                {
                    object lockk = new object();
                    Dictionary<string, MoveChain> blp = new Dictionary<string, MoveChain>();
                    int startMoveCount = board.PacnMoveList.Count;
                    ChessBoard startBoard = board.Clone();
                    Parallel.ForEach(possibleMoves, new ParallelOptions { MaxDegreeOfParallelism = parallelThreadCount }, m =>
                    {
                        if (!token.IsCancellationRequested)
                        {
                            SimpleEngine evalEngine = new SimpleEngine();
                            lock (_rsLock)
                            {
                                _runningSearches.Add(evalEngine);
                            }
                            List<string> chain;
                            if (startMoveCount == board.PacnMoveList.Count)
                            {
                                int moveEval = evalEngine.FindBestMoveChain(startBoard.CloneWithMove(m), depth, out chain);
                                if (startMoveCount == board.PacnMoveList.Count)
                                {
                                    MoveChain mc = new MoveChain { EvaluationScore = moveEval, Moves = chain };
                                    lock (lockk)
                                    {
                                        blp.Add(m, mc);
                                    }
                                }
                            }
                            lock (_rsLock)
                            {
                                _runningSearches.Remove(evalEngine);
                            }
                        }
                    });
                    if (startMoveCount == board.PacnMoveList.Count)
                        EvaluationDone(blp);
                    ++depth;
                }
            }, token);
        }

        private void ClearRunningSearches()
        {
            List<SimpleEngine> list;
            lock (_rsLock)
            {
                list = _runningSearches.ToList();
                _runningSearches.Clear();
            }
            
            foreach (SimpleEngine search in list)
                search.StopSearch = true;
        }

        private void EvaluationDone(Dictionary<string, MoveChain> blp)
        {
            lock (_blopLock)
            {
                _bestLinesOfPlay = blp;
                if (_bestLinesOfPlay.Count > 0)
                {
                    _minScore = _bestLinesOfPlay.Values.Min(v => v.EvaluationScore);
                    _maxScore = _bestLinesOfPlay.Values.Max(v => v.EvaluationScore);
                    _canSugest = true;
                }
            }
        }

        private void MoveSugestion(string move)
        {
            lock (_blopLock)
            {
                if (_canSugest && _bestLinesOfPlay.ContainsKey(move))
                {
                    _sugestionTaskCts?.Cancel();
                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        sugestionLabel.Visibility = Visibility.Hidden;
                        evaluationPanel.Visibility = Visibility.Visible;
                        betterMoveLabel.Visibility = Visibility.Hidden;
                    }));

                    const int MovePauseTimeMs = 2000;
                    //alegem lantul de mutari corespunzator
                    MoveChain chain = _bestLinesOfPlay[move];
                    depthTb.Text = Utils.GetLocalizedString("Depth", "Strings") + (chain.Moves.Count);
                    List<string> fullMoveChain = new List<string>();
                    fullMoveChain.Add(move);
                    fullMoveChain.AddRange(chain.Moves.Count > 1 ? chain.Moves.Take(chain.Moves.Count - 1) : chain.Moves);//ultima mutare din lant e de regula una rea - se ia castigul maxim: capturare
                    _auxScene.ChessBoard = _chessBoard.Clone();
                    _auxScene.PlayerPerspectiveColor = chessDisplay.PlayerRestrictionColor;
                    //culoarea textului (rosu-galben-verde) in functie de scor
                    float r, g, b;
                    GetColorForScore(chain.EvaluationScore, out r, out g, out b);
                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        expectedMovesTextBlock.Foreground = new SolidColorBrush(Color.FromScRgb(1, r, g, b));
                        expectedMovesTextBlock.Text = string.Empty;
                    }));

                    _sugestionTaskCts = new CancellationTokenSource();
                    CancellationToken token = _sugestionTaskCts.Token;
                    Task.Run(() =>
                    {
                        foreach (string m in fullMoveChain)
                        {
                            if (token.IsCancellationRequested)
                                break;
                            Dispatcher.BeginInvoke(new Action(() => { expectedMovesTextBlock.Text += " " + m; }));
                            MarkMove(m);
                            _auxScene.ChessBoard.DoFastMove(m);
                            Thread.Sleep(MovePauseTimeMs);
                        }
                    }, token);
                }
            }
        }

        private void CheckBetterMove(string move)
        {
            lock (_blopLock)
            {
                if (_canSugest && _bestLinesOfPlay.ContainsKey(move))
                {
                    MoveChain current = _bestLinesOfPlay[move];
                    float bestScore = chessDisplay.PlayerRestrictionColor == PieceColor.White ? _maxScore : _minScore;
                    float currentScore = current.EvaluationScore;
                    var bmkv = _bestLinesOfPlay.FirstOrDefault(kv => kv.Value.EvaluationScore == bestScore);
                    string text = "";
                    if (!bmkv.Equals(default(KeyValuePair<string, MoveChain>)))
                    {
                        string bestMove = _bestLinesOfPlay.First(kv => kv.Value.EvaluationScore == bestScore).Key;
                        float diff = Math.Abs(bestScore - currentScore);
                        if (diff >= 150) //mutare mult mai buna
                            text = Utils.GetLocalizedString("MuchBetterMoveText", "Strings") + bestMove;
                        else if (diff >= 80) //mutare mai buna
                            text = Utils.GetLocalizedString("BetterMoveText", "Strings") + bestMove;
                        else if (diff >= 50) //mutare putin mai buna
                            text = Utils.GetLocalizedString("SlightlyBetterMoveText", "Strings") + bestMove;
                    }
                    betterMoveLabel.Content = text;
                }
            }
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            if (!MainWindow.Pages.ContainsKey("mainmenu"))
                MainWindow.Pages["mainmenu"] = new MainMenu();
            MainWindow.Frame.Navigate(MainWindow.Pages["mainmenu"]);
        }

        private void newGameButton_Click(object sender, RoutedEventArgs e)
        {
            Windows.TrainingGameSetupWindow tgsw = new Windows.TrainingGameSetupWindow();
            if (tgsw.ShowDialog().GetValueOrDefault())
            {
                _canSugest = false;
                StopAll();
                string color = tgsw.PlayerColor;
                if (color == "random")
                    color = Utils.RandomGenerator.Next() % 2 == 0 ? "white" : "black";
                if (color == "white")
                {
                    chessDisplay.NewGame(true, PieceColor.White);
                }
                else
                {
                    chessDisplay.NewGame(true, PieceColor.Black);
                }
                if (tgsw.LimitOption == "movetime")
                {
                    _chessEngine.DepthLimit = int.MaxValue;
                    _chessEngine.MoveTime = new TimeSpan(0, 0, tgsw.MoveTime);
                }
                else if (tgsw.LimitOption == "depth")
                {
                    _chessEngine.DepthLimit = tgsw.Depth;
                    _chessEngine.MoveTime = TimeSpan.MaxValue;
                }
                else
                {
                    _chessEngine.DepthLimit = tgsw.Depth;
                    _chessEngine.MoveTime = new TimeSpan(0, 0, tgsw.MoveTime);
                }
                _chessEngine.NewGame(0,0,0,0);
                if (chessDisplay.PlayerRestrictionColor != _chessBoard.CurrentPlayerColor)
                    _chessEngine.DoFirstMove();
                demoTextBox.Visibility = Visibility.Hidden;
                chessDisplay.Visibility = Visibility.Visible;
            }
            popupMessage.Visibility = Visibility.Hidden;
        }

        private void closePopupButton_Click(object sender, RoutedEventArgs e)
        {
            popupMessage.Visibility = Visibility.Hidden;
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            _chessEngine?.Dispose();
        }

        private void MarkMove(string move)
        {
            _auxScene.MoveSquares = new List<string>
                        {
                            move.Substring(0, 2),
                            move.Substring(2, 2)
                        };
        }

        private void GetColorForScore(int score, out float r, out float g, out float b)
        {
            PieceColor playerColor = chessDisplay.PlayerRestrictionColor;
            b = 0;
            const float redYellowLength = 100;
            float max, min, value;
            if (playerColor == PieceColor.White)
            {
                max = _maxScore;
                min = _minScore;
            }
            else
            {
                max = -_minScore;
                min = -_maxScore;
                score = -score;
            }
            if (score > max - redYellowLength)
            {
                value = (score - (max - redYellowLength)) / redYellowLength;
                r = 1 - value;
                g = 1;
            }
            else
            {
                value = (score - min) / (max - redYellowLength - min);
                r = 1;
                g = value;
            }
        }

        private void stopAllButton_Click(object sender, RoutedEventArgs e)
        {
            StopAll();
            if(chessDisplay.GameInProgress)
                chessDisplay.StopGame(GameResult.Draw, null);
        }

        private void StopAll()
        {
            _sugestionTaskCts?.Cancel();
            _evaluationTaskCts?.Cancel();
            ClearRunningSearches();
        }

        #region EvaluationZone OpenGL
        private void openGLControl_OpenGLDraw(object sender, OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;
            gl.MakeCurrent();
            _auxScene?.Draw(gl);
        }

        private void openGLControl_OpenGLInitialized(object sender, OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;
            gl.MakeCurrent();
            _auxScene?.Draw(args.OpenGL);
            _auxScene?.Initialize(gl, openGLControl.ActualWidth, openGLControl.ActualHeight);
        }

        private void openGLControl_Resized(object sender, OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.MakeCurrent();
            _auxScene?.Resize(gl, openGLControl.ActualWidth, openGLControl.ActualHeight);
        }

        private void CorrectDimensions(object sender, OpenGLEventArgs args)
        {
            OpenGL gl1 = chessDisplay.OpenGLControl.OpenGL;
            OpenGL gl2 = openGLControl.OpenGL;

            gl1.MakeCurrent();
            gl1.SetDimensions((int)chessDisplay.OpenGLControl.ActualWidth, (int)chessDisplay.OpenGLControl.ActualHeight);

            gl2.MakeCurrent();
            gl2.SetDimensions((int)openGLControl.ActualWidth, (int)openGLControl.ActualHeight);
        }
        #endregion
    }

    public class MoveChain
    {
        public int EvaluationScore { get; set; }
        public List<string> Moves { get; set; }
    }
}