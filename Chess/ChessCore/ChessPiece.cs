﻿using SharpGL;
using SharpGL.SceneGraph.Assets;
using SharpGL.VertexBuffers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ChessCore
{
    public enum PieceColor
    {
        Black, White
    }

    public abstract class ChessPiece
    {
        protected ChessPiece(string position, PieceColor color)
        {
            Color = color;
            Position = position;
        }

        /// <summary>
        /// Pozitia piesei pe tabla de sah sub forma de 2 caractere "xy" unde x este litera
        /// corespunzatoare coloanei (small case) iar y cifra liniei pe care se afla.
        /// Exemplu: "e4" = coloana E, linia 4
        /// </summary>
        public string Position { get; private set; }

        /// <summary>
        /// Culoarea piesei: alb('White') sau negru('Black')
        /// </summary>
        public PieceColor Color { get; private set; }

        /// <summary>
        /// Lista tuturor mutarilor posibile pe care le poate face piesa. Lista este calculata in functie
        /// de lista pieselor aflate in joc.
        /// </summary>
        /// <param name="pieces"></param>
        /// <returns></returns>
        public List<string> PossibleMoves(IReadOnlyDictionary<string, ChessPiece> pieces)
        {
            return PseudoPossibleMoves(pieces).Where(m => CanMove(m, pieces)).ToList();
        }

        /// <summary>
        /// Lista tuturor mutarilor posibile pe care le poate face piesa.
        /// Nu se verifica daca regele este lasat in sah.
        /// Lista este calculata in functie de lista pieselor aflate in joc.
        /// </summary>
        /// <param name="pieces"></param>
        /// <returns></returns>
        public abstract List<string> PseudoPossibleMoves(IReadOnlyDictionary<string, ChessPiece> pieces);

        /// <summary>
        /// Lista pozitiilor atacate de piesa.
        /// </summary>
        /// <param name="pieces"></param>
        /// <returns></returns>
        public abstract List<string> AttackedSquares(IReadOnlyDictionary<string, ChessPiece> pieces);

        /// <summary>
        /// Returneaza litera care abreviaza numele piesei - folosita in notarea mutarilor.
        /// Ex: regina este abreviata prin litera 'Q'.
        /// </summary>
        /// <returns></returns>
        public abstract string GetLetterAbbreviation();

        /// <summary>
        /// Muta piesa pe pozitia data ca parametru.
        /// </summary>
        /// <param name="square">Pozitia pe care se muta piesa. (Ex: h7 = muta piesa pe casuta h7)</param>
        public virtual void Move(string square)
        {
            Position = square;
        }

        public abstract ChessPiece Clone();

        /// <summary>
        /// Verifica daca dupa efectuarea mutarii date ca parametru in contextul asezarii de pe tabla de saj
        /// regele nu este lasat in pozitie de saj (mutare invalida)
        /// </summary>
        /// <param name="moveTo"></param>
        /// <param name="pieces"></param>
        /// <returns></returns>
        public bool CanMove(string moveTo, IReadOnlyDictionary<string, ChessPiece> pieces)
        {
            ChessPiece tempPiece = (ChessPiece)this.MemberwiseClone();
            tempPiece.Position = moveTo;
            Dictionary<string, ChessPiece> temp = pieces.Where(kv => kv.Key != Position).ToDictionary(kv => kv.Key, kv => kv.Value);
            temp[tempPiece.Position] = tempPiece;
            PieceColor enemyColor = Color == PieceColor.White ? PieceColor.Black : PieceColor.White;
            List<ChessPiece> enemyCheckers = GetCheckers(enemyColor, temp);
            return enemyCheckers.Count == 0;
        }

        /// <summary>
        /// Returneaza lista pieselor de o anumita culoare care ataca regele advers(pozitie de sah).
        /// </summary>
        /// <param name="checkerColor">Culoarea pieselor care ataca.</param>
        /// <param name="pieces">Colectia pieselor de pe tabla de sah.</param>
        /// <returns></returns>
        public static List<ChessPiece> GetCheckers(PieceColor checkerColor, Dictionary<string, ChessPiece> pieces)
        {
            List<ChessPiece> checkers = new List<ChessPiece>();
            PieceColor kingColor = checkerColor == PieceColor.White ? PieceColor.Black : PieceColor.White;
            ChessPiece king = pieces.Values.Where(p => p is Pieces.King && p.Color == kingColor).FirstOrDefault();
            if (king != null)
            {
                string kingPosition = king.Position;
                foreach (ChessPiece p in pieces.Values)
                    if (p.Color == checkerColor && p.AttackedSquares(pieces).Contains(kingPosition))
                        checkers.Add(p);
            }
            return checkers;
        }

        #region Graphics

        /// <summary>
        /// Definitia VAO pentru desenare 2d. Este aceeasi pentru toare piesele. Unica pentru instanta OpenGL
        /// </summary>
        protected static readonly Dictionary<OpenGL, VertexBufferArray> VAOs2D = new Dictionary<OpenGL, VertexBufferArray>();
        protected static int IndexCount2d;

        /// <summary>
        /// Deseneaza grafica 2d a piesei folosind instanta OpenGL furnizata.
        /// </summary>
        /// <param name="gl"></param>
        public abstract void Draw2D(OpenGL gl);

        protected static void GenerateGeometry2D(OpenGL gl)
        {
            float[] vertices =
            {
                0, 0, 0,
                0, 1, 0,
                1, 1, 0,
                1, 0, 0
            };
            float[] texCoords =
            {
                0, 1,
                0, 0,
                1, 0,
                1, 1
            };
            float[] colors =
             {
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 0
             };
            ushort[] indexes =
            {
                0, 1, 2, 0, 2, 3
            };

            IndexCount2d = indexes.Length;

            VAOs2D[gl] = new VertexBufferArray();
            VAOs2D[gl].Create(gl);
            VAOs2D[gl].Bind(gl);

            VertexBuffer positionBuffer = new VertexBuffer();
            positionBuffer.Create(gl);
            positionBuffer.Bind(gl);
            positionBuffer.SetData(gl, System.Convert.ToUInt32(AttributeIndex.PositionAttributeIndex), vertices, false, 3);

            VertexBuffer colorBuffer = new VertexBuffer();
            colorBuffer.Create(gl);
            colorBuffer.Bind(gl);
            colorBuffer.SetData(gl, System.Convert.ToUInt32(AttributeIndex.ColorAttributeIndex), colors, false, 4);

            VertexBuffer texCoordBuffer = new VertexBuffer();
            texCoordBuffer.Create(gl);
            texCoordBuffer.Bind(gl);
            texCoordBuffer.SetData(gl, System.Convert.ToUInt32(AttributeIndex.UvAttributeIndex), texCoords, false, 2);

            IndexBuffer indexBuffer = new IndexBuffer();
            indexBuffer.Create(gl);
            indexBuffer.Bind(gl);
            indexBuffer.SetData(gl, indexes);

            VAOs2D[gl].Unbind(gl);
        }

        #endregion
    }
}
