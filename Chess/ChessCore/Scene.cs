﻿using GlmNet;
using SharpGL;
using SharpGL.Enumerations;
using SharpGL.Shaders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ChessCore
{
    public enum AttributeIndex
    {
        PositionAttributeIndex,
        ColorAttributeIndex,
        UvAttributeIndex
    }

    public class Scene
    {
        private float _drawSurfaceSize = 8.5f;
        private mat4 _projectionMatrix;
        private mat4 _viewMatrix;
        private mat4 _modelMatrix;
        private double _w, _h;

        private ShaderProgram _shaderProgram;

        public Scene(ChessBoard chessBoard)
        {
            ChessBoard = chessBoard;
            _drawSurfaceSize = ChessBoard.BOARD_SIZE;
        }

        public ChessBoard ChessBoard { get; set; }
        public List<string> AttackedSquares { get; set; }
        public List<string> MoveSquares { get; set; }
        public PieceColor PlayerPerspectiveColor { get; set; } = PieceColor.White;

        public void Draw(OpenGL gl)
        {
            if (_shaderProgram == null)
                Initialize(gl, 0, 0);

            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);

            //Desenare bordura care arata culoarea jucatorului care trebuie sa mute
            //start Imediate
            gl.MatrixMode(MatrixMode.Projection);
            gl.LoadIdentity();
            gl.Translate(0f, 0f, 1f);
            gl.Ortho(-_w / 2, _w / 2, -_h / 2, _h / 2, 0.1, 10);
            gl.MatrixMode(MatrixMode.Modelview);
            gl.LoadIdentity();
            if (ChessBoard.CurrentPlayerColor == PieceColor.White)
                gl.Color(1f, 1f, 1f, 1f);
            else
                gl.Color(0f, 0f, 0f, 1f);
            float halfSize = _drawSurfaceSize / 2;
            gl.Begin(OpenGL.GL_QUADS);
            gl.Vertex(-halfSize, -halfSize, -0.1);
            gl.Vertex(-halfSize, halfSize, -0.1);
            gl.Vertex(halfSize, halfSize, -0.1);
            gl.Vertex(halfSize, -halfSize, -0.1);
            gl.End();
            //end Imediate

            gl.Enable(OpenGL.GL_TEXTURE_2D);
            gl.Enable(OpenGL.GL_BLEND);
            _shaderProgram.Bind(gl);
            _shaderProgram.SetUniformMatrix4(gl, "projectionMatrix", _projectionMatrix.to_array());
            _shaderProgram.SetUniformMatrix4(gl, "viewMatrix", _viewMatrix.to_array());
            
            //Draw table
            _modelMatrix = new mat4(1f);
            if (PlayerPerspectiveColor == PieceColor.Black)
                _modelMatrix = _modelMatrix * glm.rotate(glm.radians(180), new vec3(0, 0, 1));
            _shaderProgram.SetUniformMatrix4(gl, "modelMatrix", _modelMatrix.to_array());
            ChessBoard.DrawBoard2D(gl);

            //DrawPieces
            foreach (ChessPiece piece in ChessBoard.Pieces.Values.ToList())
            {
                float translateX, translateY;
                char col = piece.Position.First();
                char row = piece.Position.Last();
                // indici 0...7 cu (0,0) in coltul stanga-jos din perspectiva jucatorului alb
                int rowNum = int.Parse(row.ToString()) - 1;
                int colNum = col - 'a';
                if(PlayerPerspectiveColor == PieceColor.Black)
                {
                    rowNum = 7 - rowNum;
                    colNum = 7 - colNum;
                }
                translateX = colNum - 4;
                translateY = rowNum - 4;
                _modelMatrix = glm.translate(new mat4(1f), new vec3(translateX, translateY, 0f));
                _shaderProgram.SetUniformMatrix4(gl, "modelMatrix", _modelMatrix.to_array());
                piece.Draw2D(gl);
            }

            //DrawMarkers
            if(MoveSquares != null)
                foreach(string sq in MoveSquares)
                {
                    float translateX, translateY;
                    char col = sq.First();
                    char row = sq.Last();
                    // indici 0...7 cu (0,0) in coltul stanga-jos din perspectiva jucatorului alb
                    int rowNum = int.Parse(row.ToString()) - 1;
                    int colNum = col - 'a';
                    if (PlayerPerspectiveColor == PieceColor.Black)
                    {
                        rowNum = 7 - rowNum;
                        colNum = 7 - colNum;
                    }
                    translateX = colNum - 4;
                    translateY = rowNum - 4;
                    _modelMatrix = glm.translate(new mat4(1f), new vec3(translateX, translateY, 0f));
                    _shaderProgram.SetUniformMatrix4(gl, "modelMatrix", _modelMatrix.to_array());
                    MoveSquareMarker.Draw2D(gl);
                }
            if(AttackedSquares != null)
                foreach (string sq in AttackedSquares)
                {
                    float translateX, translateY;
                    char col = sq.First();
                    char row = sq.Last();
                    // indici 0...7 cu (0,0) in coltul stanga-jos din perspectiva jucatorului alb
                    int rowNum = int.Parse(row.ToString()) - 1;
                    int colNum = col - 'a';
                    if (PlayerPerspectiveColor == PieceColor.Black)
                    {
                        rowNum = 7 - rowNum;
                        colNum = 7 - colNum;
                    }
                    translateX = colNum - 4;
                    translateY = rowNum - 4;
                    _modelMatrix = glm.translate(new mat4(1f), new vec3(translateX, translateY, 0f));
                    _shaderProgram.SetUniformMatrix4(gl, "modelMatrix", _modelMatrix.to_array());
                    AttackedSquareMarker.Draw2D(gl);
                }

            _shaderProgram.Unbind(gl);
            gl.Disable(OpenGL.GL_BLEND);
            gl.Disable(OpenGL.GL_TEXTURE_2D);
        }

        public void Initialize(OpenGL gl, double width, double height)
        {
            gl.BlendFunc(OpenGL.GL_ONE, OpenGL.GL_ONE_MINUS_SRC_ALPHA);
            //gl.ClearColor(0f, 0f, 0f, 1.0f);
            gl.ClearColor(0.4f, 0.6f, 0.9f, 0f);

            string vertexShaderSource, fragmentShaderSource;
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("ChessCore.Shaders.BasicShader.vert"))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    vertexShaderSource = reader.ReadToEnd();
                }
            }
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("ChessCore.Shaders.BasicShader.frag"))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    fragmentShaderSource = reader.ReadToEnd();
                }
            }
            Dictionary<uint, string> attributeLocations = new Dictionary<uint, string>
            {
                { System.Convert.ToUInt32(AttributeIndex.PositionAttributeIndex), "in_Position" },
                { System.Convert.ToUInt32(AttributeIndex.ColorAttributeIndex), "in_Color" },
                { System.Convert.ToUInt32(AttributeIndex.UvAttributeIndex), "in_TexCoord" }
            };

            _shaderProgram = new ShaderProgram();
            _shaderProgram.Create(gl, vertexShaderSource, fragmentShaderSource, attributeLocations);
            _shaderProgram.AssertValid(gl);

            Resize(gl, width, height);
            _viewMatrix = new mat4(1f);
            _modelMatrix = new mat4(1f);
        }

        public void Resize(OpenGL gl, double width, double height)
        {
            CreateProjectionMatrix(width, height);
            //gl.MakeCurrent();
            //gl.SetDimensions((int)width, (int)height);
        }

        public string GetSquareFromPosition(double posX, double posY, double width, double height)
        {
            double x, y;
            x = posX / width * _w;
            y = posY / height * _h;

            int col = (int)Math.Floor(x - (_w - 8) / 2);
            int row = (int)Math.Floor(y - (_h - 8) / 2);
            if (PlayerPerspectiveColor == PieceColor.Black)
            {
                row = 7 - row;
                col = 7 - col;
            }

            if (col >= 0 && col < 8 && row >= 0 && row < 8)
            {
                char colChar = (char)('a' + col);
                char rowChar = (char)('1' + 7 - row);
                return $"{colChar}{rowChar}";
            }
            else
                return null;
        }

        private void CreateProjectionMatrix(double width, double height)
        {
            if (width == 0 || height == 0)
                _w = _h = _drawSurfaceSize;
            else if (width < height)
            {
                _w = _drawSurfaceSize;
                _h = height / width * _drawSurfaceSize;
            }
            else
            {
                _h = _drawSurfaceSize;
                _w = width / height * _drawSurfaceSize;
            }
            _projectionMatrix = glm.ortho((float)-_w / 2, (float)_w / 2, (float)-_h / 2, (float)_h / 2, -10, 10);
        }
    }
}
