﻿using SharpGL;
using SharpGL.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ChessCore.WPFControls
{
    public delegate void PlayerMoveDelegate(string move);

    /// <summary>
    /// Interaction logic for ChessDisplay.xaml
    /// </summary>
    public partial class ChessDisplay : UserControl
    {
        private object glLock = new object();
        private string moveFromSquare;
        private string promoteTo;
        private Window promotionWindow;
        private PieceColor _playerRestrictionColor = PieceColor.White;

        public ChessDisplay()
        {
            InitializeComponent();

            Board = new ChessBoard();
            Board.SetupStartPosition();
            Scene = new Scene(Board);
            Board.GameEnded += StopGame;
            Clock = new Clock(Dispatcher, whiteClock, blackClock);
            Clock.TimeUp += StopGame;
        }

        public event GameEndDelegate GameEnded;

        public OpenGLControl OpenGLControl
        {
            get { return openGLControl; }
        }

        public static readonly DependencyProperty PGNProperty = DependencyProperty.Register("PGN", typeof(string), typeof(ChessDisplay), new UIPropertyMetadata(string.Empty));
        public string PGN
        {
            get { return (string)GetValue(PGNProperty); }
            set { SetValue(PGNProperty, value); }
        }

        public bool GameInProgress { get; private set; } = false;

        /// <summary>
        /// Are loc atunci cand se face o mutare interactionand cu tabla de sah.
        /// </summary>
        public event PlayerMoveDelegate PlayerMoved;

        /// <summary>
        /// Mutare facuta tinand apasat Ctrl. Nu se schimba starea tablei.
        /// </summary>
        public event PlayerMoveDelegate ControlMove;

        /// <summary>
        /// Tabla de sah care contine piesele.
        /// </summary>
        public ChessBoard Board { get; }

        /// <summary>
        /// Scena pentru reprezentarea grafica.
        /// </summary>
        public Scene Scene { get; }

        public Clock Clock { get; private set; }

        /// <summary>
        /// Arata daca mutarile facute prin interactiune cu tabla de sah sunt restrictionate numai pentru un anumit jucator
        /// (mutarile pentru celalalt jucator sunt facute cu ajutorul metodei EnemyMove).
        /// Culoarea jucatorului care poate muta este data de PlayerRestrictionColor.
        /// </summary>
        public bool PlayerRestricted { get; set; } = false;

        /// <summary>
        /// Culoarea jucatorului care poate muta prin iteractiune cu tabla de sah. Daca PlayerRestricted este false atunci
        /// aceasta restrictia nu mai are loc.
        /// </summary>
        public PieceColor PlayerRestrictionColor
        {
            get { return _playerRestrictionColor; }
            set
            {
                Scene.PlayerPerspectiveColor = value;
                _playerRestrictionColor = value;
            }
        }
        public string WhitePlayerName
        {
            get { return whitePlayerName.Text; }
            set { whitePlayerName.Text = value; }
        }
        public string BlackPlayerName
        {
            get { return blackPlayerName.Text; }
            set { blackPlayerName.Text = value; }
        }

        /// <summary>
        /// Realizeaza mutarea adversarului. Se foloseste atunci cand mutarile interactive sunt restrictionate pentru un jucator de o anumita culoare (adversarul).
        /// </summary>
        /// <param name="move"></param>
        public void EnemyMove(string move)
        {
            if (GameInProgress)
            {
                Clock.SwitchCurrentPlayer();
                Board.DoMove(move);
                UpdatePgn();
            }
        }

        public void NewGame(bool playerRestricted, PieceColor playerRestrictionColor = PieceColor.White)
        {
            Clock.Stop();
            whiteClock.Visibility = Visibility.Hidden;
            blackClock.Visibility = Visibility.Hidden;
            topSection.Children.Clear();
            bottomSection.Children.Clear();
            if (playerRestrictionColor == PieceColor.White)
            {
                topSection.Children.Add(blackBorder);
                bottomSection.Children.Add(whiteBorder);
            }
            else
            {
                topSection.Children.Add(whiteBorder);
                bottomSection.Children.Add(blackBorder);
            }
            PlayerRestricted = playerRestricted;
            PlayerRestrictionColor = playerRestrictionColor;
            Board.SetupStartPosition();
            GameInProgress = true;
            UpdatePgn();
        }

        public void NewGameWithClock(int wtime, int btime, int winc, int binc, bool playerRestricted, PieceColor playerRestrictionColor = PieceColor.White)
        {
            Clock.Restart(wtime, btime, winc, binc);
            whiteClock.Visibility = Visibility.Visible;
            blackClock.Visibility = Visibility.Visible;
            topSection.Children.Clear();
            bottomSection.Children.Clear();
            if (playerRestrictionColor == PieceColor.White)
            {
                topSection.Children.Add(blackBorder);
                bottomSection.Children.Add(whiteBorder);
            }
            else
            {
                topSection.Children.Add(whiteBorder);
                bottomSection.Children.Add(blackBorder);
            }
            PlayerRestricted = playerRestricted;
            PlayerRestrictionColor = playerRestrictionColor;
            Board.SetupStartPosition();
            GameInProgress = true;
            UpdatePgn();
        }
        public void StopGame(GameResult result, string details)
        {
            Clock.Stop();
            UpdatePgn();
            GameInProgress = false;
            GameEnded?.Invoke(result, details);
        }

        private void openGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;
            lock (glLock)
            {
                gl.MakeCurrent();
                Scene.Draw(gl);
            }
        }

        private void openGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;
            lock (glLock)
            {
                gl.MakeCurrent();
                Scene.Draw(args.OpenGL);
                Scene.Initialize(gl, openGLControl.ActualWidth, openGLControl.ActualHeight);
            }
        }

        private void openGLControl_Resized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;
            lock (glLock)
            {
                gl.MakeCurrent();
                Scene.Resize(gl, openGLControl.ActualWidth, openGLControl.ActualHeight);
            }
        }

        private void openGLControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            string sq = Scene.GetSquareFromPosition(e.GetPosition(openGLControl).X, e.GetPosition(openGLControl).Y, openGLControl.ActualWidth, openGLControl.ActualHeight);
            if (sq != null)
            {
                ChessPiece selectedPiece = Board.GetPieceOnSquare(sq);
                if (selectedPiece != null)
                {
                    moveFromSquare = sq;
                    Scene.MoveSquares = selectedPiece.PossibleMoves(Board.Pieces);
                }
                else
                {
                    moveFromSquare = null;
                    Scene.MoveSquares = null;
                }
            }
        }

        private void openGLControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            string sq = Scene.GetSquareFromPosition(e.GetPosition(openGLControl).X, e.GetPosition(openGLControl).Y, openGLControl.ActualWidth, openGLControl.ActualHeight);
            if (GameInProgress && sq != null && moveFromSquare != null)
            {
                //promotion
                ChessPiece p = Board.GetPieceOnSquare(moveFromSquare);
                if (p != null && p.PossibleMoves(Board.Pieces).Contains(sq) && (!PlayerRestricted || p.Color == PlayerRestrictionColor))
                {
                    if (p is Pieces.Pawn && (sq.Last() == '8' || sq.Last() == '1'))
                    {
                        Promotion promotionControl = new Promotion(p.Color == PieceColor.Black);
                        promotionControl.PromotionMade += PromotionControl_PromotionMade;
                        promotionWindow = new Window
                        {
                            Title = "Promotion",
                            Content = promotionControl,
                            SizeToContent = SizeToContent.WidthAndHeight,
                            ResizeMode = ResizeMode.NoResize
                        };
                        promotionWindow.ShowDialog();
                        if (promoteTo == null)
                        {
                            Scene.MoveSquares.Clear();
                            return;
                        }
                    }

                    string fullMoveNotation = moveFromSquare + sq + promoteTo;

                    if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
                        ControlMove?.Invoke(fullMoveNotation);
                    else if (Board.DoMove(fullMoveNotation))
                    {
                        Clock.SwitchCurrentPlayer();
                        UpdatePgn();
                        PlayerMoved?.Invoke(fullMoveNotation);
                    }
                }
            }

            promoteTo = null;
            moveFromSquare = null;
            Scene.MoveSquares = null;
        }

        private void PromotionControl_PromotionMade(string promoteTo)
        {
            this.promoteTo = promoteTo;
            promotionWindow.Close();
        }

        private void UpdatePgn()
        {
            Dispatcher.BeginInvoke(new Action(() => 
            {
                PGN = Board.PGN;
                movesTextBox.ScrollToEnd();
            }));
        }
    }
}
