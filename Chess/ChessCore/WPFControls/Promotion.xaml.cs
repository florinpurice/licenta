﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ChessCore.WPFControls
{
    public delegate void PromotionDelegate(string promoteTo);

    /// <summary>
    /// Interaction logic for Promotion.xaml
    /// </summary>
    public partial class Promotion : UserControl
    {
        public Promotion(bool isBlack = false)
        {
            InitializeComponent();
            Uri b, n, r, q;
            if (isBlack)
            {
                b = new Uri("/ChessCore;component/Sprites/BlackBishop.png", UriKind.Relative);
                n = new Uri("/ChessCore;component/Sprites/BlackKnight.png", UriKind.Relative);
                r = new Uri("/ChessCore;component/Sprites/BlackRook.png", UriKind.Relative);
                q = new Uri("/ChessCore;component/Sprites/BlackQueen.png", UriKind.Relative);
            }
            else
            {
                b = new Uri("/ChessCore;component/Sprites/WhiteBishop.png", UriKind.Relative);
                n = new Uri("/ChessCore;component/Sprites/WhiteKnight.png", UriKind.Relative);
                r = new Uri("/ChessCore;component/Sprites/WhiteRook.png", UriKind.Relative);
                q = new Uri("/ChessCore;component/Sprites/WhiteQueen.png", UriKind.Relative);
            }
            bishopImage.Source = new BitmapImage(b);
            knightImage.Source = new BitmapImage(n);
            rookImage.Source = new BitmapImage(r);
            queenImage.Source = new BitmapImage(q);
        }

        public event PromotionDelegate PromotionMade;

        public string Selection { get; private set; } = null;

        private void queenButton_Click(object sender, RoutedEventArgs e)
        {
            PromotionMade("q");
        }

        private void rookButton_Click(object sender, RoutedEventArgs e)
        {
            PromotionMade("r");
        }

        private void bishopButton_Click(object sender, RoutedEventArgs e)
        {
            PromotionMade("b");
        }

        private void knightButton_Click(object sender, RoutedEventArgs e)
        {
            PromotionMade("n");
        }
    }
}
