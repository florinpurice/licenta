﻿using SharpGL;
using SharpGL.SceneGraph.Assets;
using SharpGL.VertexBuffers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ChessCore
{
    public static class AttackedSquareMarker
    {
        private static readonly Dictionary<OpenGL, VertexBufferArray> vaos2D = new Dictionary<OpenGL, VertexBufferArray>();
        private static readonly Dictionary<OpenGL, Texture> textures = new Dictionary<OpenGL, Texture>();
        private static int indexCount2d;

        public static void Draw2D(OpenGL gl)
        {
            if (!textures.ContainsKey(gl))
                LoadTexture(gl);
            if (!vaos2D.ContainsKey(gl))
                GenerateGeometry2D(gl);
            textures[gl].Bind(gl);
            vaos2D[gl].Bind(gl);
            gl.DrawElements(OpenGL.GL_TRIANGLES, indexCount2d, OpenGL.GL_UNSIGNED_SHORT, IntPtr.Zero);
            vaos2D[gl].Unbind(gl);
        }

        private static void LoadTexture(OpenGL gl)
        {
            using (Stream stream = Application.GetResourceStream(new Uri("pack://application:,,,/ChessCore;component/Sprites/AttackSquare.png")).Stream)
            {
                textures[gl] = new Texture();
                Bitmap image = new Bitmap(stream);
                textures[gl].Create(gl, image);
            }
        }

        private static void GenerateGeometry2D(OpenGL gl)
        {
            float[] vertices =
            {
                0, 0, 0,
                0, 1, 0,
                1, 1, 0,
                1, 0, 0
            };
            float[] texCoords =
            {
                0, 1,
                0, 0,
                1, 0,
                1, 1
            };
            float[] colors =
             {
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 0
             };
            ushort[] indexes =
            {
                0, 1, 2, 0, 2, 3
            };

            indexCount2d = indexes.Length;

            vaos2D[gl] = new VertexBufferArray();
            vaos2D[gl].Create(gl);
            vaos2D[gl].Bind(gl);

            VertexBuffer positionBuffer = new VertexBuffer();
            positionBuffer.Create(gl);
            positionBuffer.Bind(gl);
            positionBuffer.SetData(gl, System.Convert.ToUInt32(AttributeIndex.PositionAttributeIndex), vertices, false, 3);

            VertexBuffer colorBuffer = new VertexBuffer();
            colorBuffer.Create(gl);
            colorBuffer.Bind(gl);
            colorBuffer.SetData(gl, System.Convert.ToUInt32(AttributeIndex.ColorAttributeIndex), colors, false, 4);

            VertexBuffer texCoordBuffer = new VertexBuffer();
            texCoordBuffer.Create(gl);
            texCoordBuffer.Bind(gl);
            texCoordBuffer.SetData(gl, System.Convert.ToUInt32(AttributeIndex.UvAttributeIndex), texCoords, false, 2);

            IndexBuffer indexBuffer = new IndexBuffer();
            indexBuffer.Create(gl);
            indexBuffer.Bind(gl);
            indexBuffer.SetData(gl, indexes);

            vaos2D[gl].Unbind(gl);
        }
    }
}
