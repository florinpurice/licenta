﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;

namespace ChessCore
{
    public class Clock
    {
        private static readonly List<Clock> clocks = new List<Clock>();
        private static Task tickClocksTask;
        private static object listLock = new object();
        private double moveStart;
        private DateTime moveStartTime;
        private TextBlock whiteTb, blackTb;
        private Dispatcher uiDispatcher;

        public Clock(Dispatcher uiDispatcher = null, TextBlock whiteTb = null, TextBlock blackTb = null)
        {
            this.uiDispatcher = uiDispatcher;
            this.whiteTb = whiteTb;
            this.blackTb = blackTb;
        }
        
        public event GameEndDelegate TimeUp;

        public int WhiteTime { get; private set; } = 0;
        public int BlackTime { get; private set; } = 0;
        public int WhiteIncrement { get; private set; } = 0;
        public int BlackIncrement { get; private set; } = 0;
        public PieceColor CurrentColor { get; set; } = PieceColor.White;

        public void Restart(int wtime, int btime, int winc, int binc)
        {
            Stop();
            WhiteTime = wtime;
            BlackTime = btime;
            WhiteIncrement = winc;
            BlackIncrement = binc;
            CurrentColor = PieceColor.White;
            Start();
        }

        public void Stop()
        {
            lock (listLock)
            {
                clocks.Remove(this);
            }
        }

        public void SwitchCurrentPlayer()
        {
            if (CurrentColor == PieceColor.White)
            {
                moveStart = BlackTime;
                WhiteTime += WhiteIncrement;
                CurrentColor = PieceColor.Black;
            }
            else
            {
                moveStart = WhiteTime;
                BlackTime += BlackIncrement;
                CurrentColor = PieceColor.White;
            }
            moveStartTime = DateTime.Now;
        }

        public void ChangeUIElements(Dispatcher uiDispatcher = null, TextBlock whiteTb = null, TextBlock blackTb = null)
        {
            this.uiDispatcher = uiDispatcher;
            this.whiteTb = whiteTb;
            this.blackTb = blackTb;
        }

        private static void StartTask()
        {
            if(tickClocksTask == null)
            {
                tickClocksTask = Task.Run(() =>
                {
                    while (true)
                    {
                        lock (listLock)
                        {
                            foreach (Clock clock in clocks.ToArray())
                                clock.Tick();
                        }
                        Thread.Sleep(100);
                    }
                });
            }
        }

        private void Tick()
        {
            double time = moveStart - (DateTime.Now - moveStartTime).TotalMilliseconds;
            if (CurrentColor == PieceColor.White)
            {
                WhiteTime = (int)time;
                if (WhiteTime <= 0)
                    TimeUp?.Invoke(GameResult.BlackVictory, "loss_on_time");
            }
            else
            {
                BlackTime = (int)time;
                if (BlackTime <= 0)
                    TimeUp?.Invoke(GameResult.WhiteVictory, "loss_on_time");
            }
            UpdateUI();
        }

        private void Start()
        {
            if (CurrentColor == PieceColor.White)
            {
                moveStart = WhiteTime;
                moveStartTime = DateTime.Now;
            }
            else
            {
                moveStart = BlackTime;
                moveStartTime = DateTime.Now;
            }
            lock (listLock)
            {
                clocks.Add(this);
            }
            StartTask();
        }

        private void UpdateUI()
        {
            if(uiDispatcher != null && whiteTb != null && blackTb != null)
            {
                int wmsec, wsec, wmin;
                wmsec = (int)WhiteTime;
                wsec = wmsec / 1000;
                wmsec %= 1000;
                wmin = wsec / 60;
                wsec %= 60;
                string wstring = $"{wmin}:{wsec:D2}";
                if (wmin == 0 && wsec < 10)
                    wstring += $".{wmsec/100}";

                int bmsec, bsec, bmin;
                bmsec = (int)BlackTime;
                bsec = bmsec / 1000;
                bmsec %= 1000;
                bmin = bsec / 60;
                bsec %= 60;
                string bstring = $"{bmin}:{bsec:D2}";
                if (bmin == 0 && bsec < 10)
                    bstring += $".{bmsec / 100}";

                try
                {
                    uiDispatcher.BeginInvoke(new Action(() =>
                    {
                        whiteTb.Text = wstring;
                        blackTb.Text = bstring;
                    }));
                }
                catch { }
            }
        }
    }
}
