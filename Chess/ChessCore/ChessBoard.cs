﻿using SharpGL;
using SharpGL.SceneGraph.Assets;
using SharpGL.VertexBuffers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ChessCore
{
    public delegate void GameEndDelegate(GameResult result, string details);

    public enum GameResult
    {
        Draw,
        WhiteVictory,
        BlackVictory
    }

    public class ChessBoard
    {
        private readonly Dictionary<string, ChessPiece> _pieces = new Dictionary<string, ChessPiece>();
        private readonly Dictionary<string, int> _positionCount = new Dictionary<string, int>();
        private int _staleHalfMovesCount = 0;
        /// <summary>
        /// Lista mutarilor in notatie PACN (Pure Algebraic Coordinate Notation)
        /// </summary>
        public readonly List<string> PacnMoveList = new List<string>();
        /// <summary>
        /// Lista mutarilor in notatie PGN (Portable Game Notation)
        /// </summary>
        public readonly List<string> PgnMoveList = new List<string>();

        /// <summary>
        /// Lista pieselor aflate in joc.
        /// </summary>
        public IReadOnlyDictionary<string, ChessPiece> Pieces
        {
            get
            {
                return new ReadOnlyDictionary<string, ChessPiece>(_pieces);
            }
        }
        public string PGN
        {
            get
            {
                StringBuilder pgn = new StringBuilder();
                for(int i = 0; i < PgnMoveList.Count; ++i)
                {
                    if (i % 2 == 0)
                        pgn.Append(string.Format("{0,-3} {1,-7}", (i / 2 + 1).ToString() + ".", PgnMoveList[i]));
                    else
                        pgn.Append($" {PgnMoveList[i]}\n");
                }
                return pgn.ToString();
            }
        }
        public string FEN
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                var pieces = from p in _pieces.Values.ToList()
                             select new { rank = p.Position.Last(), file = p.Position.First(), piece = p };
                var ordered = pieces.OrderByDescending(p => p.rank).ThenBy(p => p.file).ToList();
                if (ordered.Count > 0)
                {
                    char r = '8';
                    var p = ordered[0];
                    int index = 0;
                    while (r > '0')
                    {
                        int blanks = 0;
                        char f = 'a';
                        while (f < 'i')
                        {
                            if (p.rank == r && p.file == f)
                            {
                                if (blanks > 0)
                                    sb.Append(blanks);
                                blanks = 0;
                                string pieceNotation = p.piece.GetLetterAbbreviation();
                                if (string.IsNullOrEmpty(pieceNotation))
                                    pieceNotation = "P";
                                if (p.piece.Color == PieceColor.Black)
                                    pieceNotation = pieceNotation.ToLower();
                                sb.Append(pieceNotation);
                                if (++index < ordered.Count)
                                    p = ordered[index];
                            }
                            else
                                ++blanks;
                            f = (char)(f + 1);
                        }
                        if (blanks > 0)
                            sb.Append(blanks);
                        if (r > '1')
                            sb.Append('/');
                        r = (char)(r - 1);
                    }
                }
                else
                    sb.Append("8/8/8/8/8/8/8/8");
                if (CurrentPlayerColor == PieceColor.White)
                    sb.Append(" w");
                else
                    sb.Append(" b");
                return sb.ToString();
            }
        }
        public event GameEndDelegate GameEnded;
        public PieceColor CurrentPlayerColor { get; set; } = PieceColor.White;
        public void Clear()
        {
            _pieces.Clear();
            _positionCount.Clear();
            PacnMoveList.Clear();
            PgnMoveList.Clear();
            _staleHalfMovesCount = 0;
        }

        /// <summary>
        /// Face mutarea data ca parametru.
        /// </summary>
        /// <param name="move">Mutare reprezentata ca sir de patru caracter. Ex: "a1b3" = muta piesa
        /// de pe pozitia a1 pe pozitia b3.</param>
        /// <returns>true = mutare efectuata cu succes, false = mutare invalida</returns>
        public bool DoMove(string move)
        {
            if (move.Length < 4)
                return false;
            string fromSquare = move.Substring(0, 2);
            string toSquare = move.Substring(2, 2);
            ChessPiece piece = null;
            if (_pieces.ContainsKey(fromSquare))
                piece = _pieces[fromSquare];
            if (piece != null 
                && piece.Color == CurrentPlayerColor 
                && piece.PossibleMoves(Pieces).Contains(toSquare))
            {
                bool isStaleMove = true;//nu se captureza nici o piesa si nu se muta nici un pion; pentru regula de 50 de mutari.
                if (_pieces.ContainsKey(toSquare) || piece is Pieces.Pawn)
                    isStaleMove = false;
                if (isStaleMove)
                    ++_staleHalfMovesCount;
                else
                    _staleHalfMovesCount = 0;
                //pregatire notare PGN, in caz de rocada, promovare sau sah/sah-mat aceasta mutare va fi modificata mai tarziu.
                string fromNotation = piece.GetLetterAbbreviation();
                string toNotation = toSquare;
                string fullPgn = null;
                Type pieceType = piece.GetType();
                if (_pieces.Values.Any(p => p != piece && p.GetType() == pieceType && p.Color == piece.Color && p.PossibleMoves(Pieces).Contains(toSquare)))
                {
                    //notarea fara precizarea casutei de plecare este ambigua, trebuie specificata coloana, linia sau ambele.
                    if(_pieces.Values.Any(p => p != piece && p.Position.First() == fromSquare.First() && p.GetType() == pieceType && p.Color == piece.Color && p.PossibleMoves(Pieces).Contains(toSquare)))
                    {
                        //nu se poate departaja doar dupa coloana
                        if (_pieces.Values.Any(p => p != piece && p.Position.Last() == fromSquare.Last() && p.GetType() == pieceType && p.Color == piece.Color && p.PossibleMoves(Pieces).Contains(toSquare)))
                        {
                            //nu se poate departaja doar dupa linie, se precizeaza coordonatele complete
                            fromNotation += fromSquare;
                        }
                        else
                        {
                            //se poate departaja doar dupa linie
                            fromNotation += fromSquare.Last();
                        }
                    }
                    else
                    {
                        //se poate departaja doar dupa coloana
                        fromNotation += fromSquare.First();
                    }
                }
                if (_pieces.ContainsKey(toSquare))
                    toNotation = 'x' + toNotation;

                //Castling
                if (piece is Pieces.King)
                {
                    char c1 = fromSquare.First();
                    char c2 = toSquare.First();
                    char r = fromSquare.Last();
                    if (c2 - c1 == 2)//rocada mica
                    {
                        ChessPiece rook = _pieces[$"h{r}"];
                        _pieces.Remove(rook.Position);
                        rook.Move($"f{r}");
                        _pieces[rook.Position] = rook;

                        fullPgn = "O-O"; //notare PGN pentru rocada mica
                    }
                    else if(c2 - c1 == -2)//rocada mare
                    {
                        ChessPiece rook = _pieces[$"a{r}"];
                        _pieces.Remove(rook.Position);
                        rook.Move($"d{r}");
                        _pieces[rook.Position] = rook;

                        fullPgn = "O-O-O"; //notare PGN pentru rocada mare
                    }
                }

                //En-passant
                if (piece is Pieces.Pawn)
                {
                    char c1 = fromSquare.First();
                    char c2 = toSquare.First();
                    if(c1 - c2 != 0 && !_pieces.ContainsKey(toSquare))
                    {
                        char r = fromSquare.Last();
                        _pieces.Remove($"{c2}{r}");

                        toNotation = 'x' + toNotation; //capturare prin en-passant se noteaza normal, cu x
                    }
                }

                //Promotion
                if (piece is Pieces.Pawn && (toSquare.Last() == '8' || toSquare.Last() == '1'))
                    if (move.Length == 5 && "qrbn".Contains(move.Last()))
                    {
                        ChessPiece newPiece;
                        switch (move.Last())
                        {
                            case 'q':
                                {
                                    newPiece = new Pieces.Queen(piece.Position, piece.Color);
                                    toNotation += "=Q";
                                    break;
                                }
                            case 'r':
                                {
                                    newPiece = new Pieces.Rook(piece.Position, piece.Color);
                                    toNotation += "=R";
                                    break;
                                }
                            case 'b':
                                {
                                    newPiece = new Pieces.Bishop(piece.Position, piece.Color);
                                    toNotation += "=B";
                                    break;
                                }
                            case 'n':
                                {
                                    newPiece = new Pieces.Knight(piece.Position, piece.Color);
                                    toNotation += "=N";
                                    break;
                                }
                            default: return false;
                        }
                        piece = newPiece;
                        _pieces[piece.Position] = piece;
                    }
                    else
                        return false;

                ClearPawnFlag();
                _pieces.Remove(piece.Position);
                piece.Move(toSquare);
                _pieces[piece.Position] = piece;
                CurrentPlayerColor = CurrentPlayerColor == PieceColor.White ? PieceColor.Black : PieceColor.White;

                if (string.IsNullOrEmpty(fromNotation) && toNotation.StartsWith("x"))
                    fromNotation = fromSquare.First().ToString(); //can pionul captureaza alta piesa, chiar si in caz in care nu exista ambiguitate, trebuie precizata coloana
                PieceColor checkerColor = CurrentPlayerColor == PieceColor.White ? PieceColor.Black : PieceColor.White;
                if (ChessPiece.GetCheckers(checkerColor, _pieces).Any())
                    toNotation += '+';
                if (fullPgn == null)
                    fullPgn = fromNotation + toNotation;
                PgnMoveList.Add(fullPgn);
                PacnMoveList.Add(move);
                string fen = FEN;
                if (_positionCount.ContainsKey(fen))
                    _positionCount[fen] = _positionCount[fen] + 1;
                else
                    _positionCount[fen] = 1;

                GameResult temp;
                CheckIfGameEnded(out temp);

                return true;
            }
            else
                return false;
        }

        public bool CheckIfGameEnded(out GameResult res)
        {
            res = GameResult.Draw;
            //50 moves rule
            if(_staleHalfMovesCount/2 > 49)
            {
                GameEnded?.Invoke(GameResult.Draw, "fifty_moves_rule");
                return true;
            }

            //threefold repetition
            if(_positionCount.Values.Any(v => v > 2))
            {
                GameEnded?.Invoke(GameResult.Draw, "threefold_repetition");
                return true;
            }

            //verifica daca este sah-mat sau pat/stalemate (jucatorul curent nu poate face nici o mutare legala-pat-
            //iar daca pe deasupra regele este atacat atunci e sah-mat
            if(_pieces.Values
                .Where(p => p.Color == CurrentPlayerColor)
                .All(p => p.PossibleMoves(_pieces).Count == 0))
            {
                PieceColor checkerColor = CurrentPlayerColor == PieceColor.White ? PieceColor.Black : PieceColor.White;
                if (ChessPiece.GetCheckers(checkerColor, _pieces).Any())
                //checkmate
                {
                    //adaugam '#' la ultima mutare din notare PGN, semnaland ca a fost mutare de sah-mat
                    if (PgnMoveList.Count > 0)
                    {
                        string lastMove = PgnMoveList.Last();
                        PgnMoveList.RemoveAt(PgnMoveList.Count - 1);
                        lastMove = lastMove.Substring(0, lastMove.Length - 1) + '#';
                        PgnMoveList.Add(lastMove);
                    }

                    if (CurrentPlayerColor == PieceColor.White)
                        res = GameResult.BlackVictory;
                    else
                        res = GameResult.WhiteVictory;
                    GameEnded?.Invoke(res, "checkmate");
                    return true;
                }
                else //stalemate
                {
                    GameEnded?.Invoke(GameResult.Draw, "stalemate");
                    return true;
                }
            }

            //piese insuficiente pentru sah-mat
            bool insufficient = true;
            string[] sufficientPieces = { "p", "P", "q", "Q", "r", "R" };
            string[] whiteMinor = { "B", "N" };
            string[] blackMinor = { "b", "n" };
            int whiteMinorCount = 0, blackMinorCount = 0;
            string[] simpleBoard = GetSimpleBoardNotation();
            foreach(string s in simpleBoard)
                if(s != null)
                {
                    if (sufficientPieces.Contains(s))
                    {
                        insufficient = false;
                        break;
                    }
                    if (whiteMinor.Contains(s))
                        ++whiteMinorCount;
                    else if (blackMinor.Contains(s))
                        ++blackMinorCount;
                }
            if (whiteMinorCount > 1 || blackMinorCount > 1)
                insufficient = false;
            if(insufficient)
            {
                GameEnded?.Invoke(GameResult.Draw, "insufficient_material");
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returneaza piesa de pe o anumita pozitie.
        /// </summary>
        /// <param name="square"></param>
        /// <returns>Piesa de pe pozitia data sau null daca pe acea pozitie nu se afla nici o piesa</returns>
        public ChessPiece GetPieceOnSquare(string square)
        {
            if (_pieces.ContainsKey(square))
                return _pieces[square];
            else
                return null;
        }

        /// <summary>
        /// Plaseaza piesa pe tabla pe pozitia data de proprietatea Position (piece.Position).
        /// Daca pe acea pozitie se afla deja alta piesa, acea piesa va fi inlocuita.
        /// </summary>
        /// <param name="piece"></param>
        public void PlacePiece(ChessPiece piece)
        {
            _pieces[piece.Position] = piece;
        }

        public void SetupStartPosition()
        {
            Clear();

            PlacePiece(new Pieces.Pawn("a2", PieceColor.White));
            PlacePiece(new Pieces.Pawn("b2", PieceColor.White));
            PlacePiece(new Pieces.Pawn("c2", PieceColor.White));
            PlacePiece(new Pieces.Pawn("d2", PieceColor.White));
            PlacePiece(new Pieces.Pawn("e2", PieceColor.White));
            PlacePiece(new Pieces.Pawn("f2", PieceColor.White));
            PlacePiece(new Pieces.Pawn("g2", PieceColor.White));
            PlacePiece(new Pieces.Pawn("h2", PieceColor.White));
            PlacePiece(new Pieces.Rook("a1", PieceColor.White));
            PlacePiece(new Pieces.Knight("b1", PieceColor.White));
            PlacePiece(new Pieces.Bishop("c1", PieceColor.White));
            PlacePiece(new Pieces.Queen("d1", PieceColor.White));
            PlacePiece(new Pieces.King("e1", PieceColor.White));
            PlacePiece(new Pieces.Bishop("f1", PieceColor.White));
            PlacePiece(new Pieces.Knight("g1", PieceColor.White));
            PlacePiece(new Pieces.Rook("h1", PieceColor.White));

            PlacePiece(new Pieces.Pawn("a7", PieceColor.Black));
            PlacePiece(new Pieces.Pawn("b7", PieceColor.Black));
            PlacePiece(new Pieces.Pawn("c7", PieceColor.Black));
            PlacePiece(new Pieces.Pawn("d7", PieceColor.Black));
            PlacePiece(new Pieces.Pawn("e7", PieceColor.Black));
            PlacePiece(new Pieces.Pawn("f7", PieceColor.Black));
            PlacePiece(new Pieces.Pawn("g7", PieceColor.Black));
            PlacePiece(new Pieces.Pawn("h7", PieceColor.Black));
            PlacePiece(new Pieces.Rook("a8", PieceColor.Black));
            PlacePiece(new Pieces.Knight("b8", PieceColor.Black));
            PlacePiece(new Pieces.Bishop("c8", PieceColor.Black));
            PlacePiece(new Pieces.Queen("d8", PieceColor.Black));
            PlacePiece(new Pieces.King("e8", PieceColor.Black));
            PlacePiece(new Pieces.Bishop("f8", PieceColor.Black));
            PlacePiece(new Pieces.Knight("g8", PieceColor.Black));
            PlacePiece(new Pieces.Rook("h8", PieceColor.Black));

            CurrentPlayerColor = PieceColor.White;
            _positionCount[FEN] = 1;
        }

        public ChessBoard Clone()
        {
            ChessBoard clone = new ChessBoard();
            foreach (var p in _pieces)
                clone._pieces.Add(p.Key, p.Value.Clone());
            foreach (var s in _positionCount)
                clone._positionCount.Add(s.Key, s.Value);
            clone._staleHalfMovesCount = _staleHalfMovesCount;
            clone.PacnMoveList.AddRange(PacnMoveList);
            clone.PgnMoveList.AddRange(PgnMoveList);
            clone.CurrentPlayerColor = CurrentPlayerColor;
            return clone;
        }

        public ChessBoard CloneWithMove(string move)
        {
            ChessBoard clone = Clone();
            clone.DoFastMove(move);
            return clone;
        }

        public string[] GetSimpleBoardNotation()
        {
            string[] boardState = new string[64];
            foreach(ChessPiece piece in _pieces.Values.ToList())
            {
                int row = '8' - piece.Position.Last();
                int col = piece.Position.First() - 'a';
                int arrayPos = row * 8 + col;
                string letterAbbr = piece.GetLetterAbbreviation();
                if (string.IsNullOrEmpty(letterAbbr))
                    letterAbbr = "P";
                if (piece.Color == PieceColor.Black)
                    letterAbbr = letterAbbr.ToLower();
                boardState[arrayPos] = letterAbbr;
            }
            return boardState;
        }

        /// <summary>
        /// Realizeaza mutarea fara prea multe verificari. Folosita pt cautarea mutarii deoarece este mai rapida decat DoMove.
        /// </summary>
        /// <param name="move"></param>
        public void DoFastMove(string move)
        {
            string fromSquare = move.Substring(0, 2);
            string toSquare = move.Substring(2, 2);
            ChessPiece piece = _pieces[fromSquare];
            PieceColor pieceColor = piece.Color;
            string pieceNotation = piece.GetLetterAbbreviation();
            PacnMoveList.Add(move);

            //rocada
            if(pieceNotation == "K")
            {
                char c1 = fromSquare.First();
                char c2 = toSquare.First();
                int diff = c2 - c1;
                if(diff == 2)//rocada mica
                {
                    string rookfrom = null;
                    string rookto = null;
                    if (pieceColor == PieceColor.White)
                    {
                        rookfrom = "h1";
                        rookto = "f1";
                    }
                    else
                    {
                        rookfrom = "h8";
                        rookto = "f8";
                    }
                    ChessPiece rook = _pieces[rookfrom];
                    _pieces.Remove(rookfrom);
                    rook.Move(rookto);
                    _pieces[rookto] = rook;
                }
                else if(diff == -2)//rocada mare
                {
                    string rookfrom = null;
                    string rookto = null;
                    if (pieceColor == PieceColor.White)
                    {
                        rookfrom = "a1";
                        rookto = "d1";
                    }
                    else
                    {
                        rookfrom = "a8";
                        rookto = "d8";
                    }
                    ChessPiece rook = _pieces[rookfrom];
                    _pieces.Remove(rookfrom);
                    rook.Move(rookto);
                    _pieces[rookto] = rook;
                }
            }

            if(pieceNotation == "")//pion
            {
                char c1 = fromSquare.First();
                char c2 = toSquare.First();
                if (c1 - c2 != 0 && !_pieces.ContainsKey(toSquare))//en-passant
                {
                    char r = fromSquare.Last();
                    _pieces.Remove($"{c2}{r}");
                }

                if(move.Length == 5)//promotion
                    switch (move.Last())
                    {
                        case 'q':
                            {
                                piece = new Pieces.Queen(piece.Position, piece.Color);
                                break;
                            }
                        case 'r':
                            {
                                piece = new Pieces.Rook(piece.Position, piece.Color);
                                break;
                            }
                        case 'b':
                            {
                                piece = new Pieces.Bishop(piece.Position, piece.Color);
                                break;
                            }
                        case 'n':
                            {
                                piece = new Pieces.Knight(piece.Position, piece.Color);
                                break;
                            }
                    }
            }

            ClearPawnFlag();
            _pieces.Remove(fromSquare);
            piece.Move(toSquare);
            _pieces[toSquare] = piece;
            CurrentPlayerColor = CurrentPlayerColor == PieceColor.White ? PieceColor.Black : PieceColor.White;
        }

        private void ClearPawnFlag()
        {
            foreach (Pieces.Pawn pawn in _pieces.Values.OfType<Pieces.Pawn>())
                pawn.JumpedTwo = false;
        }

        #region Graphics

        private static readonly Dictionary<OpenGL, VertexBufferArray> vaos2D = new Dictionary<OpenGL, VertexBufferArray>();
        private static readonly Dictionary<OpenGL, Texture> textures = new Dictionary<OpenGL, Texture>();
        private static int indexCount2d;

        public const float BOARD_SIZE = 8.714893617021277f;

        public static void DrawBoard2D(OpenGL gl)
        {
            if (!textures.ContainsKey(gl))
                LoadTexture(gl);
            if (!vaos2D.ContainsKey(gl))
                GenerateGeometry2D(gl);
            textures[gl].Bind(gl);
            vaos2D[gl].Bind(gl);
            gl.DrawElements(OpenGL.GL_TRIANGLES, indexCount2d, OpenGL.GL_UNSIGNED_SHORT, IntPtr.Zero);
            vaos2D[gl].Unbind(gl);
        }

        private static void LoadTexture(OpenGL gl)
        {
            using (Stream stream = Application.GetResourceStream(new Uri("pack://application:,,,/ChessCore;component/Sprites/Chessboard.png")).Stream)
            {
                textures[gl] = new Texture();
                Bitmap image = new Bitmap(stream);
                textures[gl].Create(gl, image);
            }
        }

        private static void GenerateGeometry2D(OpenGL gl)
        {
            float halfSize = BOARD_SIZE / 2;
            float[] vertices =
            {
                -halfSize, -halfSize, 0,
                -halfSize, halfSize, 0,
                halfSize, halfSize, 0,
                halfSize, -halfSize, 0
            };
            float[] texCoords =
            {
                0, 1,
                0, 0,
                1, 0,
                1, 1
            };
            float[] colors =
             {
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 0
             };
            ushort[] indexes =
            {
                0, 1, 2, 0, 2, 3
            };

            indexCount2d = indexes.Length;

            vaos2D[gl] = new VertexBufferArray();
            vaos2D[gl].Create(gl);
            vaos2D[gl].Bind(gl);

            VertexBuffer positionBuffer = new VertexBuffer();
            positionBuffer.Create(gl);
            positionBuffer.Bind(gl);
            positionBuffer.SetData(gl, System.Convert.ToUInt32(AttributeIndex.PositionAttributeIndex), vertices, false, 3);

            VertexBuffer colorBuffer = new VertexBuffer();
            colorBuffer.Create(gl);
            colorBuffer.Bind(gl);
            colorBuffer.SetData(gl, System.Convert.ToUInt32(AttributeIndex.ColorAttributeIndex), colors, false, 4);

            VertexBuffer texCoordBuffer = new VertexBuffer();
            texCoordBuffer.Create(gl);
            texCoordBuffer.Bind(gl);
            texCoordBuffer.SetData(gl, System.Convert.ToUInt32(AttributeIndex.UvAttributeIndex), texCoords, false, 2);

            IndexBuffer indexBuffer = new IndexBuffer();
            indexBuffer.Create(gl);
            indexBuffer.Bind(gl);
            indexBuffer.SetData(gl, indexes);

            vaos2D[gl].Unbind(gl);
        }
        #endregion
    }
}
