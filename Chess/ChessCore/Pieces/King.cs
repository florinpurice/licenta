﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpGL;
using SharpGL.SceneGraph.Assets;
using System.IO;
using System.Drawing;
using System.Windows;

namespace ChessCore.Pieces
{
    public class King : ChessPiece
    {
        public King(string position, PieceColor color) : base(position, color) { }

        public override void Move(string square)
        {
            base.Move(square);
            DidMove = true;
        }

        public bool DidMove { get; set; } = false;

        public override List<string> AttackedSquares(IReadOnlyDictionary<string, ChessPiece> pieces)
        {
            char col = Position.First();
            int row = int.Parse(Position.Last().ToString());
            int colNum = col - 'a' + 1;
            int c, r;
            List<string> squareList = new List<string>();
            string sq;

            //sw
            c = colNum - 1;
            r = row - 1;
            if(c > 0 && r > 0)
            {
                sq = $"{(char)(c - 1 + 'a')}{r}";
                squareList.Add(sq);
            }

            //se
            c = colNum + 1;
            r = row - 1;
            if (c < 9 && r > 0)
            {
                sq = $"{(char)(c - 1 + 'a')}{r}";
                squareList.Add(sq);
            }

            //nw
            c = colNum - 1;
            r = row + 1;
            if (c > 0 && r < 9)
            {
                sq = $"{(char)(c - 1 + 'a')}{r}";
                squareList.Add(sq);
            }

            //ne
            c = colNum + 1;
            r = row + 1;
            if (c < 9 && r < 9)
            {
                sq = $"{(char)(c - 1 + 'a')}{r}";
                squareList.Add(sq);
            }

            //w
            c = colNum - 1;
            r = row;
            if (c > 0)
            {
                sq = $"{(char)(c - 1 + 'a')}{r}";
                squareList.Add(sq);
            }

            //e
            c = colNum + 1;
            r = row;
            if (c < 9)
            {
                sq = $"{(char)(c - 1 + 'a')}{r}";
                squareList.Add(sq);
            }

            //s
            c = colNum;
            r = row - 1;
            if (r > 0)
            {
                sq = $"{(char)(c - 1 + 'a')}{r}";
                squareList.Add(sq);
            }

            //n
            c = colNum;
            r = row + 1;
            if (r < 9)
            {
                sq = $"{(char)(c - 1 + 'a')}{r}";
                squareList.Add(sq);
            }

            return squareList;
        }

        public override List<string> PseudoPossibleMoves(IReadOnlyDictionary<string, ChessPiece> pieces)
        {
            List<string> squareList = new List<string>();
            char col = Position.First();
            if (!DidMove && col - 'e' == 0 && CanMove(Position, pieces))
            {
                char row = Position.Last();
                string rookAPos = $"a{row}";
                string rookHPos = $"h{row}";
                //rocada mare
                if (pieces.ContainsKey(rookAPos) &&
                    pieces[rookAPos] is Rook &&
                    !(pieces[rookAPos] as Rook).DidMove &&
                    !pieces.ContainsKey($"b{row}") &&
                    !pieces.ContainsKey($"c{row}") &&
                    !pieces.ContainsKey($"d{row}") &&
                    CanMove($"c{row}", pieces) &&
                    CanMove($"d{row}", pieces))
                    squareList.Add($"c{row}");
                //rocada mica
                if (pieces.ContainsKey(rookHPos) &&
                    pieces[rookHPos] is Rook &&
                    !(pieces[rookHPos] as Rook).DidMove &&
                    !pieces.ContainsKey($"f{row}") &&
                    !pieces.ContainsKey($"g{row}") &&
                    CanMove($"f{row}", pieces) &&
                    CanMove($"g{row}", pieces))
                    squareList.Add($"g{row}");
            }

            return AttackedSquares(pieces)
                .Where(sq => !pieces.ContainsKey(sq) || pieces[sq].Color != Color).Union(squareList).ToList();
        }

        public override string GetLetterAbbreviation()
        {
            return "K";
        }

        public override ChessPiece Clone()
        {
            return new King(Position, Color) { DidMove = this.DidMove };
        }
        #region Graphics

        private static readonly Dictionary<OpenGL, Texture> _whiteTextures2d = new Dictionary<OpenGL, Texture>();
        private static readonly Dictionary<OpenGL, Texture> _blackTextures2d = new Dictionary<OpenGL, Texture>();

        public override void Draw2D(OpenGL gl)
        {
            Texture texture;
            if (Color == PieceColor.White)
            {
                if (!_whiteTextures2d.ContainsKey(gl))
                    LoadTextures(gl);
                texture = _whiteTextures2d[gl];
            }
            else
            {
                if (!_blackTextures2d.ContainsKey(gl))
                    LoadTextures(gl);
                texture = _blackTextures2d[gl];
            }
            if (!VAOs2D.ContainsKey(gl))
                GenerateGeometry2D(gl);
            texture.Bind(gl);
            VAOs2D[gl].Bind(gl);
            gl.DrawElements(OpenGL.GL_TRIANGLES, IndexCount2d, OpenGL.GL_UNSIGNED_SHORT, IntPtr.Zero);
            VAOs2D[gl].Unbind(gl);
        }

        private static void LoadTextures(OpenGL gl)
        {
            using (Stream stream = Application.GetResourceStream(new Uri("pack://application:,,,/ChessCore;component/Sprites/WhiteKing.png")).Stream)
            {
                _whiteTextures2d[gl] = new Texture();
                Bitmap image = new Bitmap(stream);
                _whiteTextures2d[gl].Create(gl, image);
            }

            using (Stream stream = Application.GetResourceStream(new Uri("pack://application:,,,/ChessCore;component/Sprites/BlackKing.png")).Stream)
            {
                _blackTextures2d[gl] = new Texture();
                Bitmap image = new Bitmap(stream);
                _blackTextures2d[gl].Create(gl, image);
            }
        }

        #endregion
    }
}
