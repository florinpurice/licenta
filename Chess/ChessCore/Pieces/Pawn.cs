﻿using SharpGL;
using SharpGL.SceneGraph.Assets;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ChessCore.Pieces
{
    public class Pawn : ChessPiece
    {
        public Pawn(string position, PieceColor color) : base(position, color) { }

        public override void Move(string square)
        {
            char r1 = Position.Last();
            char r2 = square.Last();
            int diff = Math.Abs(r1 - r2);
            if (diff == 2)
                JumpedTwo = true;
            base.Move(square);
        }

        /// <summary>
        /// Arata daca pionul a sarit doua pozitii (de pe rangul 2 pe 4 sau de pe 7 pe 5; poate fi luat en-passant
        /// tura viitoare de catre un pion inamic adiacent pe rang (4, respectiv 5).
        /// </summary>
        public bool JumpedTwo { get; set; } = false;

        public override List<string> AttackedSquares(IReadOnlyDictionary<string, ChessPiece> pieces)
        {
            char col = Position.First();
            int row = int.Parse(Position.Last().ToString());
            List<string> squareList = new List<string>();
            if(Color == PieceColor.White)
            {
                if(row < 8)
                {
                    char c1 = (char)(col - 1);
                    char c2 = (char)(col + 1);
                    if (c1 >= 'a')
                        squareList.Add($"{c1}{row + 1}");
                    if (c2 <= 'h')
                        squareList.Add($"{c2}{row + 1}");
                }
            }
            else
            {
                if(row > 1)
                {
                    char c1 = (char)(col - 1);
                    char c2 = (char)(col + 1);
                    if (c1 >= 'a')
                        squareList.Add($"{c1}{row - 1}");
                    if (c2 <= 'h')
                        squareList.Add($"{c2}{row - 1}");
                }
            }
            return squareList;
        }

        public override List<string> PseudoPossibleMoves(IReadOnlyDictionary<string, ChessPiece> pieces)
        {
            char col = Position.First();
            int row = int.Parse(Position.Last().ToString());
            List<string> squareList = new List<string>();

            List<string> attList = AttackedSquares(pieces);
            foreach (string pos in attList)
                if (pieces.ContainsKey(pos) && pieces[pos].Color != Color)
                    squareList.Add(pos);

            if (Color == PieceColor.White)
            {
                if (row < 8)
                {
                    string fwd = $"{col}{row + 1}";
                    if (!pieces.ContainsKey(fwd))
                    {
                        squareList.Add(fwd);
                        if(row == 2)
                        {
                            string fwd2 = $"{col}{row + 2}";
                            if (!pieces.ContainsKey(fwd2))
                                squareList.Add(fwd2);
                        }
                    }
                }
                if(row == 5)
                {
                    char c1 = (char)(col - 1);
                    char c2 = (char)(col + 1);
                    string fleft = $"{c1}6";
                    string left = $"{c1}5";
                    string fright = $"{c2}6";
                    string right = $"{c2}5";
                    if (!squareList.Contains(fleft) &&
                        !pieces.ContainsKey(fleft) &&
                        pieces.ContainsKey(left) &&
                        pieces[left].Color != Color &&
                        pieces[left] is Pawn &&
                        (pieces[left] as Pawn).JumpedTwo)
                        squareList.Add(fleft);
                    if (!squareList.Contains(fright) &&
                        !pieces.ContainsKey(fright) &&
                        pieces.ContainsKey(right) &&
                        pieces[right].Color != Color &&
                        pieces[right] is Pawn &&
                        (pieces[right] as Pawn).JumpedTwo)
                        squareList.Add(fright);
                }
            }
            else
            {
                if (row > 1)
                {
                    string fwd = $"{col}{row - 1}";
                    if (!pieces.ContainsKey(fwd))
                    {
                        squareList.Add(fwd);
                        if (row == 7)
                        {
                            string fwd2 = $"{col}{row - 2}";
                            if (!pieces.ContainsKey(fwd2))
                                squareList.Add(fwd2);
                        }
                    }
                }
                if (row == 4)
                {
                    char c1 = (char)(col + 1);
                    char c2 = (char)(col - 1);
                    string fleft = $"{c1}3";
                    string left = $"{c1}4";
                    string fright = $"{c2}3";
                    string right = $"{c2}4";
                    if (!squareList.Contains(fleft) &&
                        !pieces.ContainsKey(fleft) &&
                        pieces.ContainsKey(left) &&
                        pieces[left].Color != Color &&
                        pieces[left] is Pawn &&
                        (pieces[left] as Pawn).JumpedTwo)
                        squareList.Add(fleft);
                    if (!squareList.Contains(fright) &&
                        !pieces.ContainsKey(fright) &&
                        pieces.ContainsKey(right) &&
                        pieces[right].Color != Color &&
                        pieces[right] is Pawn &&
                        (pieces[right] as Pawn).JumpedTwo)
                        squareList.Add(fright);
                }
            }

            return squareList.ToList();
        }

        public override string GetLetterAbbreviation()
        {
            return "";
        }

        public override ChessPiece Clone()
        {
            return new Pawn(Position, Color) { JumpedTwo = this.JumpedTwo };
        }
        #region Graphics

        private static readonly Dictionary<OpenGL, Texture> _whiteTextures2d = new Dictionary<OpenGL, Texture>();
        private static readonly Dictionary<OpenGL, Texture> _blackTextures2d = new Dictionary<OpenGL, Texture>();

        public override void Draw2D(OpenGL gl)
        {
            Texture texture;
            if (Color == PieceColor.White)
            {
                if (!_whiteTextures2d.ContainsKey(gl))
                    LoadTextures(gl);
                texture = _whiteTextures2d[gl];
            }
            else
            {
                if (!_blackTextures2d.ContainsKey(gl))
                    LoadTextures(gl);
                texture = _blackTextures2d[gl];
            }
            if (!VAOs2D.ContainsKey(gl))
                GenerateGeometry2D(gl);
            texture.Bind(gl);
            VAOs2D[gl].Bind(gl);
            gl.DrawElements(OpenGL.GL_TRIANGLES, IndexCount2d, OpenGL.GL_UNSIGNED_SHORT, IntPtr.Zero);
            VAOs2D[gl].Unbind(gl);
        }

        private static void LoadTextures(OpenGL gl)
        {
            using (Stream stream = Application.GetResourceStream(new Uri("pack://application:,,,/ChessCore;component/Sprites/WhitePawn.png")).Stream)
            {
                _whiteTextures2d[gl] = new Texture();
                Bitmap image = new Bitmap(stream);
                _whiteTextures2d[gl].Create(gl, image);
            }

            using (Stream stream = Application.GetResourceStream(new Uri("pack://application:,,,/ChessCore;component/Sprites/BlackPawn.png")).Stream)
            {
                _blackTextures2d[gl] = new Texture();
                Bitmap image = new Bitmap(stream);
                _blackTextures2d[gl].Create(gl, image);
            }
        }

        #endregion
    }
}
