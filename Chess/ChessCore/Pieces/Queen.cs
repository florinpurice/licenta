﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpGL;
using SharpGL.SceneGraph.Assets;
using System.IO;
using System.Drawing;
using System.Windows;

namespace ChessCore.Pieces
{
    public class Queen : ChessPiece
    {

        public Queen(string position, PieceColor color) : base(position, color) { }

        public override List<string> AttackedSquares(IReadOnlyDictionary<string, ChessPiece> pieces)
        {
            char col = Position.First();
            int row = int.Parse(Position.Last().ToString());
            int colNum = col - 'a' + 1;
            int c, r;
            List<string> squareList = new List<string>();
            string sq;

            //sw
            c = colNum - 1;
            r = row - 1;
            while (r > 0 && c > 0)
            {
                sq = $"{(char)(c - 1 + 'a')}{r}";
                squareList.Add(sq);
                if (pieces.ContainsKey(sq))
                    break;
                --r; --c;
            }

            //se
            c = colNum + 1;
            r = row - 1;
            while (r > 0 && c < 9)
            {
                sq = $"{(char)(c - 1 + 'a')}{r}";
                squareList.Add(sq);
                if (pieces.ContainsKey(sq))
                    break;
                --r; ++c;
            }

            //nw
            c = colNum - 1;
            r = row + 1;
            while (r < 9 && c > 0)
            {
                sq = $"{(char)(c - 1 + 'a')}{r}";
                squareList.Add(sq);
                if (pieces.ContainsKey(sq))
                    break;
                ++r; --c;
            }

            //ne
            c = colNum + 1;
            r = row + 1;
            while (r < 9 && c < 9)
            {
                sq = $"{(char)(c - 1 + 'a')}{r}";
                squareList.Add(sq);
                if (pieces.ContainsKey(sq))
                    break;
                ++r; ++c;
            }

            //w
            c = colNum - 1;
            r = row;
            while (c > 0)
            {
                sq = $"{(char)(c - 1 + 'a')}{r}";
                squareList.Add(sq);
                if (pieces.ContainsKey(sq))
                    break;
                --c;
            }

            //e
            c = colNum + 1;
            r = row;
            while (c < 9)
            {
                sq = $"{(char)(c - 1 + 'a')}{r}";
                squareList.Add(sq);
                if (pieces.ContainsKey(sq))
                    break;
                ++c;
            }

            //s
            c = colNum;
            r = row - 1;
            while (r > 0)
            {
                sq = $"{(char)(c - 1 + 'a')}{r}";
                squareList.Add(sq);
                if (pieces.ContainsKey(sq))
                    break;
                --r;
            }

            //n
            c = colNum;
            r = row + 1;
            while (r < 9)
            {
                sq = $"{(char)(c - 1 + 'a')}{r}";
                squareList.Add(sq);
                if (pieces.ContainsKey(sq))
                    break;
                ++r;
            }

            return squareList;
        }

        public override List<string> PseudoPossibleMoves(IReadOnlyDictionary<string, ChessPiece> pieces)
        {
            return AttackedSquares(pieces)
                .Where(sq => !pieces.ContainsKey(sq) || pieces[sq].Color != Color).ToList();
        }

        public override string GetLetterAbbreviation()
        {
            return "Q";
        }

        public override ChessPiece Clone()
        {
            return new Queen(Position, Color);
        }
        #region Graphics

        private static readonly Dictionary<OpenGL, Texture> _whiteTextures2d = new Dictionary<OpenGL, Texture>();
        private static readonly Dictionary<OpenGL, Texture> _blackTextures2d = new Dictionary<OpenGL, Texture>();

        public override void Draw2D(OpenGL gl)
        {
            Texture texture;
            if (Color == PieceColor.White)
            {
                if (!_whiteTextures2d.ContainsKey(gl))
                    LoadTextures(gl);
                texture = _whiteTextures2d[gl];
            }
            else
            {
                if (!_blackTextures2d.ContainsKey(gl))
                    LoadTextures(gl);
                texture = _blackTextures2d[gl];
            }
            if (!VAOs2D.ContainsKey(gl))
                GenerateGeometry2D(gl);
            texture.Bind(gl);
            VAOs2D[gl].Bind(gl);
            gl.DrawElements(OpenGL.GL_TRIANGLES, IndexCount2d, OpenGL.GL_UNSIGNED_SHORT, IntPtr.Zero);
            VAOs2D[gl].Unbind(gl);
        }
        private static void LoadTextures(OpenGL gl)
        {
            using (Stream stream = Application.GetResourceStream(new Uri("pack://application:,,,/ChessCore;component/Sprites/WhiteQueen.png")).Stream)
            {
                _whiteTextures2d[gl] = new Texture();
                Bitmap image = new Bitmap(stream);
                _whiteTextures2d[gl].Create(gl, image);
            }

            using (Stream stream = Application.GetResourceStream(new Uri("pack://application:,,,/ChessCore;component/Sprites/BlackQueen.png")).Stream)
            {
                _blackTextures2d[gl] = new Texture();
                Bitmap image = new Bitmap(stream);
                _blackTextures2d[gl].Create(gl, image);
            }
        }

        #endregion
    }
}
