﻿#version 150 core

in vec4 pass_Color;
in vec2 pass_TexCoord;

uniform sampler2D diffuse;

void main(void) {
	if(pass_Color == vec4(0, 0, 0, 0))
		gl_FragColor = texture(diffuse, pass_TexCoord);
	else
		gl_FragColor = pass_Color;
	gl_FragColor.rgb *= gl_FragColor.a;
}